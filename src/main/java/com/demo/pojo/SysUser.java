package com.demo.pojo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>..................
 *
 * @author ${author}
 * @since 2020-12-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SysUser对象", description="")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private String id;

    @ApiModelProperty(value = "账户名称")
    private String username;

    @ApiModelProperty(value = "加密盐值")
    private String salt;

    @ApiModelProperty(value = "用户密码明文")
    private String password;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty("所属公司名称")
    private String deptName;

    @ApiModelProperty(value = "真实名称")
    private String realName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "邮箱(唯一)")
    private String email;

    @ApiModelProperty(value = "账户状态(1.正常 2.锁定)")
    private Integer status;

    @ApiModelProperty(value = "性别(1.男 2.女)")
    private Integer sex;

    @ApiModelProperty(value = "是否删除(0未删除；1已删除)")
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "创建id")
    private String createId;

    @ApiModelProperty(value = "更新id")
    private String updateId;

    @ApiModelProperty("删除id")
    @TableField(exist = false)
    private String deletedId;

    @ApiModelProperty(value = "创建来源(1.web 2.android 3.ios )")
    private Integer createWhere;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
