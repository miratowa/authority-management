package com.demo.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 轮播图表结构
 * </p>
 *
 * @author ${author}
 * @since 2021-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SysRotationChart对象", description="轮播图表结构")
public class SysRotationChart implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "轮播图主键id")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private String id;

    @ApiModelProperty(value = "轮播图广告跳转地址")
    private String url;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "图片文件地址")
    private String fileUrl;

    @ApiModelProperty(value = "排序位置")
    private Integer sort;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "创建用户id")
    private String createId;

    @ApiModelProperty(value = "更新用户id")
    private String updateId;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}
