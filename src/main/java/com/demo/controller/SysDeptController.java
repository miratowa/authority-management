package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.pojo.SysDept;
import com.demo.service.SysDeptService;
import com.demo.util.DataResult;
import com.demo.vo.req.AddDeptReqVO;
import com.demo.vo.req.UpdateDeptReqVO;
import com.demo.vo.resp.DeptRespNodeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/api")
@Api(tags = "组织管理——部门管理")
public class SysDeptController {
    @Autowired
    private SysDeptService deptService;

    @GetMapping("/depts")
    @ApiOperation("跳转到部门管理页面")
    @MyLog(title = "组织管理——部门管理",action = "跳转到部门管理页面")
    //查询部门信息列表接口权限
    @RequiresPermissions("sys:dept:list")
    public DataResult<List<SysDept>> depts() {
        return new DataResult(deptService.selectAll());
    }

    @GetMapping("/dept/tree")
    @ApiOperation("获取部门结构树")
    @MyLog(title = "组织管理——部门管理",action = "获取部门结构树")
    //部门树权限
    @RequiresPermissions(value = {"sys:user:update","sys:user:add","sys:dept:add","sys:dept:update"},logical = Logical.OR)
    public DataResult<List<DeptRespNodeVO>> getDeptTree(@RequestParam(required = false) String deptId) {
        DataResult result = DataResult.success();
        result.setData(deptService.getDeptTree(deptId));
        return result;
    }

    @PostMapping("/dept/add")
    @ApiOperation("添加部门")
    @MyLog(title = "组织管理——部门管理",action = "添加部门")
    //新增部门权限
    @RequiresPermissions("sys:dept:add")
    public DataResult<SysDept> addDept(@Valid @RequestBody AddDeptReqVO vo) {
        return new DataResult<>(deptService.addDept(vo));
    }

    @PutMapping("/dept/update")
    @ApiOperation("编辑部门")
    @MyLog(title = "组织管理——部门管理",action = "编辑部门")
    //更新部门信息权限
    @RequiresPermissions("sys:dept:update")
    public DataResult updateDept(@Valid @RequestBody UpdateDeptReqVO vo) {
        deptService.updateDept(vo);
        DataResult result = DataResult.success();
        return result;
    }

    @DeleteMapping("/dept/delete/{deptId}")
    @ApiOperation("删除部门")
    @MyLog(title = "组织管理——部门管理",action = "删除部门")
    //删除部门信息权限
    @RequiresPermissions("sys:dept:delete")
    public DataResult deleteDept(@PathVariable("deptId") String deptId) {
        deptService.deleteDept(deptId);
        DataResult result = DataResult.success();
        return result;
    }
}

