package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.contants.Constant;
import com.demo.pojo.SysRotationChart;
import com.demo.service.SysRotationChartService;
import com.demo.util.DataResult;
import com.demo.util.JWTUtil;
import com.demo.vo.req.AddRotationChartReqAddVO;
import com.demo.vo.req.RotationPageReqVO;
import com.demo.vo.req.UpdateRotationChartReqVO;
import com.demo.vo.resp.PageInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.crypto.Data;
import java.util.List;

/**
 * <p>
 * 轮播图表结构 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-02-01
 */
@RestController
@RequestMapping("/api")
@Api(tags = "轮播图功能相关接口")
public class SysRotationChartController {
    @Autowired
    private SysRotationChartService rotationChartService;

    @PostMapping("/rotations")
    @ApiOperation("分页查询轮播图信息")
    @MyLog(title = "轮播图功能相关接口",action = "分页查询轮播图信息")
    @RequiresPermissions("sys:rotation:list")
    public DataResult<PageInfoVO<SysRotationChart>> pageInfo(@RequestBody RotationPageReqVO vo) {
        DataResult result = DataResult.success();
        result.setData(rotationChartService.pageInfo(vo));
        return result;
    }

    @PostMapping("/rotations/add")
    @ApiOperation("添加轮播图接口")
    @MyLog(title = "轮播图功能相关接口", action = "添加轮播图接口")
    @RequiresPermissions("sys:rotation:add")
    public DataResult addRotation(@RequestBody @Valid AddRotationChartReqAddVO vo, HttpServletRequest req) {
        String userId = JWTUtil.getUserId(req.getHeader(Constant.ACCESS_TOKEN));
        rotationChartService.addRotationChart(vo, userId);
        return DataResult.success();
    }

    @PutMapping("/rotations/update")
    @ApiOperation("编辑轮播图接口")
    @MyLog(title = "轮播图功能相关接口", action = "编辑轮播图接口")
    @RequiresPermissions("sys:rotation:update")
    public DataResult updateRotation(@RequestBody @Valid UpdateRotationChartReqVO vo, HttpServletRequest req) {
        String userId = JWTUtil.getUserId(req.getHeader(Constant.ACCESS_TOKEN));
        rotationChartService.updateRotationChart(vo, userId);
        return DataResult.success();
    }

    @DeleteMapping("/rotations/delete")
    @ApiOperation("批量删除轮播图接口")
    @MyLog(title = "轮播图功能相关接口", action = "批量删除轮播图接口")
    @RequiresPermissions("sys:rotation:delete")
    public DataResult deleteRotation(@RequestBody List<String> ids) {
        rotationChartService.deleteRotationChartById(ids);
        return DataResult.success();
    }

    @GetMapping("/rotations")
    @ApiOperation("轮播图前端页面展示")
    public DataResult<List<SysRotationChart>> selectAll() {
        DataResult result = DataResult.success();
        result.setData(rotationChartService.selectAll());
        return result;
    }
}

