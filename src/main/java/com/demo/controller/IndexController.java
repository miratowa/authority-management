package com.demo.controller;

import com.demo.service.SysUserService;
import com.demo.util.DataResult;
import com.demo.vo.req.LoginReqVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * @description: 视图控制器
 * @create: 2020-12-29 15:24
 **/
@Controller
@Api(tags = "视图控制器")
public class IndexController {

    @GetMapping({"/", "index"})
    @ApiOperation("登录页面")
    public String toLoginPage() {
        return "login";
    }

    @GetMapping("/404")
    @ApiOperation("跳转到404错误页面")
    public String error404() {
        return "error/404";
    }

    @GetMapping("/toHome")
    @ApiOperation("跳转到首页")
    public String toHome() {
        return "home";
    }

    @GetMapping("/main")
    @ApiOperation("跳转到导航栏的每个子页面")
    public String toMain() {
        return "main";
    }

    @GetMapping("/menus")
    @ApiOperation("跳转到菜单权限管理页面")
    public String toMenus() {
        return "menus/menu";
    }

    @GetMapping("/roles")
    @ApiOperation("跳转到角色管理页面")
    public String toRoles() {
        return "roles/role";
    }

    @GetMapping("/depts")
    @ApiOperation("跳转到部门信息页面")
    public String toDepts() {
        return "depts/dept";
    }

    @GetMapping("/users")
    @ApiOperation("跳转到用户管理页面")
    public String toUsers() {
        return "users/user";
    }

    @GetMapping("/logs")
    @ApiOperation("跳转到用户日志页面")
    public String toLogs() {
        return "logs/log";
    }

    @GetMapping("/users/info")
    @ApiOperation(value = "跳转个人用户信息编辑页面")
    public String userDetail(){
        return "users/user_edit";
    }

    @GetMapping("/users/password")
    @ApiOperation(value = "跳转个人用户编辑密码页面")
    public String userPwd(){
        return "users/user_pwd";
    }

    @GetMapping("/rotations/rotation")
    @ApiOperation(value = "跳转到轮播图管理页面")
    public String rotation(){
        return "rotations/rotation";
    }

    @GetMapping("/rotations/show")
    @ApiOperation(value = "轮播图页面展现")
    public String rotationShow(){
        return "rotations/rotation_show";
    }

    @GetMapping("/files")
    @ApiOperation(value = "跳转到我的文件页面")
    public String files(){
        return "files/my_file";
    }

}
