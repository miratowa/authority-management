package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.pojo.SysLog;
import com.demo.service.SysLogService;
import com.demo.util.DataResult;
import com.demo.vo.req.LogPageReqVO;
import com.demo.vo.resp.PageInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统日志 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/api")
@Api(tags = "系统管理——用户日志管理")
public class SysLogController {
    @Autowired
    private SysLogService logService;

    @PostMapping("/logs")
    @ApiOperation(value = "分页查询系统操作日志接口")
    @MyLog(title = "系统管理——用户日志管理", action = "分页查询系统操作日志")
    //日志列表接口权限
    @RequiresPermissions("sys:log:list")
    public DataResult<PageInfoVO<SysLog>> pageInfo(@RequestBody LogPageReqVO vo) {
        PageInfoVO<SysLog> pageInfo = logService.pageInfo(vo);
        DataResult result = DataResult.success();
        result.setData(pageInfo);
        return result;
    }

    @DeleteMapping("/logs/delete")
    @ApiOperation("批量删除用户日志")
    @MyLog(title = "系统管理——用户日志管理", action = "批量删除用户日志")
    //删除日志接口权限
    @RequiresPermissions("sys:log:delete")
    public DataResult deleteLogs(@RequestBody List<String> logIds) {
        logService.deleteLogsByLogIds(logIds);
        DataResult result = DataResult.success();
        return result;
    }

}

