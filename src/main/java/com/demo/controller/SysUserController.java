package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.contants.Constant;
import com.demo.pojo.SysUser;
import com.demo.service.SysUserService;
import com.demo.util.DataResult;
import com.demo.util.JWTUtil;
import com.demo.vo.req.*;
import com.demo.vo.resp.LoginRespVO;
import com.demo.vo.resp.PageInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Api(tags = "组织管理——用户模块")
@Validated
@Slf4j
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/login")
    @ApiOperation("登录方法")
    @MyLog(title = "组织管理——用户模块", action = "登录方法")
    public DataResult<LoginRespVO> login(@Valid @RequestBody LoginReqVO vo) {
        DataResult result = DataResult.success();
        result.setData(sysUserService.login(vo));
        return result;
    }

    @GetMapping("/user/logout")
    @ApiOperation("退出登录")
    @MyLog(title = "组织管理——用户模块", action = "退出登录")
    public DataResult logout(HttpServletRequest req) {
        try {
            String accessToken = req.getHeader(Constant.ACCESS_TOKEN);
            String refreshToken = req.getHeader(Constant.REFRESH_TOKEN);
            sysUserService.logout(accessToken, refreshToken);
        } catch (Exception e) {
            log.error("logout error{}", e);
        }
        return DataResult.success();
    }

    @PostMapping("/users")
    @ApiOperation("获取所有角色信息")
    @MyLog(title = "组织管理——用户模块", action = "获取所有角色信息")
    //1查询用户接口
    @RequiresPermissions("sys:user:list")
    public DataResult<PageInfoVO<SysUser>> usersPage(@RequestBody UserPageReqVO vo) {
        return new DataResult(sysUserService.pageInfo(vo));
    }

    @PostMapping("/user/add")
    @ApiModelProperty("添加用户")
    @MyLog(title = "组织管理——用户模块", action = "添加用户")
    //2新增用户权限
    @RequiresPermissions("sys:user:add")
    public DataResult addUser(@Valid @RequestBody AddUserReqVO vo, HttpServletRequest req) {
        return new DataResult(sysUserService.addUser(vo, JWTUtil.getUserId(req.getHeader(Constant.ACCESS_TOKEN))));
    }

    @GetMapping("/user/token")
    @ApiModelProperty("自动刷新token")
    @MyLog(title = "组织管理——用户模块", action = "自动刷新token")
    public DataResult<String> refreshToken(HttpServletRequest request) {
        // 获取原先的token
        String token = request.getHeader(Constant.ACCESS_TOKEN);
        return DataResult.success(sysUserService.refreshToken(token));
    }

    @PutMapping("/user/update")
    @ApiOperation("编辑用户信息")
    @MyLog(title = "组织管理——用户模块", action = "编辑用户信息")
    //4列表编辑用户信息权限
    @RequiresPermissions("sys:user:update")
    public DataResult updateUser(@Valid @RequestBody UpdateUserReqVO vo, HttpServletRequest request) {
        String operationId = JWTUtil.getUserId(request.getHeader(Constant.ACCESS_TOKEN));
        DataResult result = DataResult.success();
        sysUserService.UpdateUser(vo, operationId);
        return result;
    }

    @DeleteMapping("/users/delete")
    @ApiOperation("批量删除用户")
    @MyLog(title = "组织管理——用户模块", action = "批量删除用户")
    //5删除用户权限
    @RequiresPermissions("sys:user:delete")
    public DataResult deletedUsers(@RequestBody @ApiParam("用户id集合") List<String> userIds, HttpServletRequest req) {
        sysUserService.DeleteUsers(userIds);
        return DataResult.success();
    }

    @GetMapping("/user/info")
    @ApiOperation("获取用户详情信息")
    @MyLog(title = "组织管理——用户模块", action = "获取用户详情信息")
    public DataResult detailInfo(HttpServletRequest req) {
        SysUser user = sysUserService.detailInfo(JWTUtil.getUserId(req.getHeader(Constant.ACCESS_TOKEN)));
        return DataResult.success(user);
    }

    @PutMapping("/user/info")
    @ApiOperation("完善用户个人信息")
    @MyLog(title = "组织管理——用户模块", action = "完善用户个人信息")
    public DataResult updateDetailInfo(HttpServletRequest req, @RequestBody UpdateUserDetailInfoReqVO vo) {
        String accessToken = req.getHeader(Constant.ACCESS_TOKEN);
        String userId = JWTUtil.getUserId(accessToken);
        sysUserService.updateDetailInfo(userId, vo);
        return DataResult.success();
    }

    @PutMapping("/user/pwd")
    @ApiOperation("修改用户登录密码")
    @MyLog(title = "组织管理——用户模块", action = "修改用户登录密码")
    public DataResult updateUserPwd(@RequestBody UpdateUserPwdReqVO vo, HttpServletRequest req) {
        String accessToken = req.getHeader(Constant.ACCESS_TOKEN);
        String refreshToken = req.getHeader(Constant.REFRESH_TOKEN);
        String userId = JWTUtil.getUserId(accessToken);
        sysUserService.updateUserPwd(userId, accessToken, refreshToken, vo);
        return DataResult.success();
    }
}

