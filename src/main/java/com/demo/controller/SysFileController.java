package com.demo.controller;


import com.demo.contants.Constant;
import com.demo.pojo.SysFile;
import com.demo.service.SysFileService;
import com.demo.util.DataResult;
import com.demo.util.JWTUtil;
import com.demo.vo.req.FilePageReqVO;
import com.demo.vo.resp.PageInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 文件信息存储表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-01-31
 */
@RestController
@RequestMapping("/api")
@Api(tags = "文件操作相关接口")
public class SysFileController {
    @Autowired
    private SysFileService fileService;

    @PostMapping("/file")
    @ApiOperation("文件上传接口")
    public DataResult fileUpload(@RequestParam("file") MultipartFile file, HttpServletRequest req) {
        String accessToken = req.getHeader(Constant.ACCESS_TOKEN);
        int fileType = req.getIntHeader(Constant.FILE_TYPE);
        String userId = JWTUtil.getUserId(accessToken);
        return DataResult.success(fileService.fileUpload(file, userId, fileType));
    }

    @GetMapping("/file/download/{fileId}")
    @ApiOperation("文件下载接口")
    public DataResult fileDownload(@PathVariable("fileId") String fileId, HttpServletResponse resp) {
        fileService.downloadFile(fileId, resp);
        return DataResult.success();
    }

    @PostMapping("/files")
    @ApiOperation("分页获取当前用户的文件接口")
    public DataResult<PageInfoVO<SysFile>> pageInfo(@RequestBody FilePageReqVO vo, HttpServletRequest req) {
        String userId = JWTUtil.getUserId(req.getHeader(Constant.ACCESS_TOKEN));
        DataResult result = DataResult.success();
        result.setData(fileService.pageInfo(vo, userId));
        return result;
    }
}

