package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.pojo.SysPermission;
import com.demo.service.SysPermissionService;
import com.demo.util.DataResult;
import com.demo.vo.req.AddPermissionReqVO;
import com.demo.vo.req.UpdatePermissionReqVO;
import com.demo.vo.resp.PermissionRespNodeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/api")
@Api(tags = "组织管理——权限菜单管理")
public class SysPermissionController {
    @Autowired
    private SysPermissionService permissionService;

    @GetMapping("/permission")
    @ApiOperation("获取菜单列表")
    @MyLog(title = "组织管理——权限菜单管理",action = "获取菜单列表")
    //菜单权限列表权限
    @RequiresPermissions("sys:permission:list")
    public DataResult<SysPermission> getPermissionInfo() {
        return DataResult.success(permissionService.selectAll());
    }

    @GetMapping("/permission/tree")
    @ApiOperation("获取权限菜单树（只递归查询到菜单栏）")
    @MyLog(title = "组织管理——权限菜单管理",action = "获取权限菜单树（只递归查询到菜单栏）")
    //菜单权限树接口权限(查询到目录)
    @RequiresPermissions(value = {"sys:permission:update","sys:permission:add"},logical = Logical.OR)
    public DataResult<List<PermissionRespNodeVO>> getPermissionTree() {
        return DataResult.success(permissionService.selectAllMenuByTree());
    }

    @PostMapping("/addPermission")
    @ApiOperation("添加权限菜单")
    @MyLog(title = "组织管理——权限菜单管理",action = "添加权限菜单")
    //新增菜单权限接口
    @RequiresPermissions("sys:permission:add")
    public DataResult<SysPermission> addPermission(@Valid @RequestBody AddPermissionReqVO vo) {
        return new DataResult(permissionService.addPermission(vo));
    }

    @GetMapping("/permission/tree/all")
    @ApiOperation("递归出所有的权限菜单树")
    @MyLog(title = "组织管理——权限菜单管理",action = "递归出所有的权限菜单树")
    //菜单权限树接口权限(查询到按钮)
    @RequiresPermissions(value = {"sys:role:update","sys:role:add"},logical = Logical.OR)
    public DataResult getPermissionAllTree() {
        return DataResult.success(permissionService.getPermissionAllTree());
    }

    @PutMapping("/permission/update")
    @ApiOperation("编辑菜单权限")
    @MyLog(title = "组织管理——权限菜单管理",action = "编辑菜单权限")
    //更新菜单权限接口权限
    @RequiresPermissions("sys:permission:update")
    public DataResult updatePermission(@Valid @RequestBody UpdatePermissionReqVO vo) {
        permissionService.updatePermission(vo);
        return DataResult.success();
    }

    @DeleteMapping("/permission/{permissionId}")
    @ApiOperation("删除菜单权限")
    @MyLog(title = "组织管理——权限菜单管理",action = "删除菜单权限")
    //删除菜单权限接口权限
    @RequiresPermissions("sys:permission:delete")
    public DataResult deletePermission(@PathVariable("permissionId") String permissionId) {
        permissionService.deletePermissionByPermissionId(permissionId);
        return DataResult.success();
    }
}

