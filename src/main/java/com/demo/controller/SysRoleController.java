package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.mapper.SysRoleMapper;
import com.demo.pojo.SysRole;
import com.demo.pojo.SysRolePermission;
import com.demo.service.SysRolePermissionService;
import com.demo.service.SysRoleService;
import com.demo.util.DataResult;
import com.demo.vo.req.AddRoleVO;
import com.demo.vo.req.RolePageReqVO;
import com.demo.vo.req.UpdateRoleReqVO;
import com.demo.vo.resp.PageInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/api")
@Api(tags = "组织管理——角色管理")
public class SysRoleController {

    @Autowired
    private SysRoleService roleService;

    @PostMapping("/roles")
    @ApiOperation("获取所有角色信息")
    @MyLog(title = "组织管理——角色管理",action = "获取所有角色信息")
    //角色列表接口权限
    @RequiresPermissions("sys:role:list")
    public DataResult<PageInfoVO<SysRole>> rolePageInfo(@RequestBody RolePageReqVO vo) {
        DataResult<SysRole> result = new DataResult<>();
        return result.success(roleService.pageInfo(vo));
    }

    @PostMapping("/addRole")
    @ApiOperation("新增角色接口")
    @MyLog(title = "组织管理——角色管理",action = "新增角色接口")
    //新增角色接口权限
    @RequiresPermissions("sys:role:add")
    public DataResult addRole(@Valid @RequestBody AddRoleVO vo) {
        return new DataResult(roleService.addRole(vo));
    }

    @GetMapping("/role/{id}")
    @ApiOperation("获取角色详情接口")
    @MyLog(title = "组织管理——角色管理",action = "获取角色详情接口")
    //查询角色详情接口权限
    @RequiresPermissions("sys:role:detail")
    public DataResult<SysRole> getDetailInfo(@PathVariable("id") String id) {
        SysRole sysRole = roleService.detailInfo(id);
        return DataResult.success(sysRole);
    }

    @PutMapping("/updateRole")
    @ApiOperation("编辑角色接口")
    @MyLog(title = "组织管理——角色管理",action = "编辑角色接口")
    //更新角色信息接口权限
    @RequiresPermissions("sys:role:update")
    public DataResult<SysRole> updateRole(@Valid @RequestBody UpdateRoleReqVO vo) {
        DataResult result = DataResult.success();
        roleService.updateRoleByRoleId(vo);
        return result;
    }

    @DeleteMapping("/deleteRole/{roleId}")
    @ApiOperation("删除角色接口")
    @MyLog(title = "组织管理——角色管理",action = "删除角色接口")
    //删除角色接口权限
    @RequiresPermissions("sys:role:delete")
    public DataResult deleteRole(@PathVariable("roleId") String roleId) {
        DataResult result = DataResult.success();
        roleService.deleteRoleByRoleId(roleId);
        return result;
    }
}
