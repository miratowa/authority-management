package com.demo.controller;

import com.demo.aop.annotation.MyLog;
import com.demo.contants.Constant;
import com.demo.service.SysUserService;
import com.demo.util.DataResult;
import com.demo.util.JWTUtil;
import com.demo.vo.resp.HomeRespVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @description:
 * @create: 2020-12-30 09:08
 **/
@RestController
@RequestMapping("/api")
@Api(tags = "首页模块")
public class HomeController {
    @Autowired
    private SysUserService userService;

    @GetMapping("/home")
    @ApiOperation("显示用户数据和导航栏")
    @MyLog(title = "首页模块",action = "显示用户数据和导航栏")
    public DataResult home(HttpServletRequest request) {
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        // 解析出用户id
        String userId = JWTUtil.getUserId(accessToken);
        // 获取用户信息
        DataResult<HomeRespVO> result = new DataResult<>(userService.getHomeInfo(userId));
        return result;
    }

}
