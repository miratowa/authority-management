package com.demo.controller;


import com.demo.aop.annotation.MyLog;
import com.demo.service.SysUserRoleService;
import com.demo.service.SysUserService;
import com.demo.util.DataResult;
import com.demo.vo.req.AssignUserRolesReqVO;
import com.demo.vo.resp.UserOwnRoleRespVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/api")
@Api(tags = "组织管理——角色用户授权管理")
public class SysUserRoleController {

    @Autowired
    private SysUserRoleService userRoleService;

    @GetMapping("/user/roles/{userId}")
    @ApiModelProperty("查询用户拥有的角色数据")
    @MyLog(title = "组织管理——角色用户授权管理",action = "查询用户拥有的角色数据")
    public DataResult<UserOwnRoleRespVO> getUserOwnRole(@PathVariable("userId") String userId) {
        return new DataResult(userRoleService.getUserOwnRole(userId));
    }

    @PutMapping("/assign/user/roles")
    @ApiModelProperty("赋予用户角色接口")
    @MyLog(title = "组织管理——角色用户授权管理",action = "赋予用户角色接口")
    //3赋予用户角色权限
    @RequiresPermissions("sys:user:role:update")
    public DataResult assignUserRoles(@Valid @RequestBody AssignUserRolesReqVO vo) {
        DataResult result = DataResult.success();
        userRoleService.AssignUserRoles(vo);
        return result;
    }
}

