package com.demo.service;

import com.demo.pojo.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.LogPageReqVO;
import com.demo.vo.resp.PageInfoVO;

import java.util.List;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
public interface SysLogService extends IService<SysLog> {
    /**
     * @description: 分页查询用户操作的日志数据
     * @param vo
     * @return: com.demo.vo.resp.PageInfoVO<com.demo.pojo.SysLog>
     */
    PageInfoVO<SysLog> pageInfo(LogPageReqVO vo);

    /**
     * @description: 批量删除用户操作日志
     * @param logIds
     * @return: int
     */
    int deleteLogsByLogIds(List<String> logIds);
}
