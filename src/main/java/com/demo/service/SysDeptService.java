package com.demo.service;

import com.demo.pojo.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.AddDeptReqVO;
import com.demo.vo.req.UpdateDeptReqVO;
import com.demo.vo.resp.DeptRespNodeVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
public interface SysDeptService extends IService<SysDept> {
    /**
     * @description: 查询所有部门信息
     * @return: java.util.List<com.demo.pojo.SysDept>
     */
    List<SysDept> selectAll();

    /**
     * @description: 获取部门树结构 
     * @return: java.util.List<com.demo.pojo.SysDept>
     */
    List<DeptRespNodeVO> getDeptTree(String deptId);

    /**
     * @description: 添加部门
     * @param vo
     * @return: com.demo.pojo.SysDept
     */
    SysDept addDept(AddDeptReqVO vo);

    /**
     * @description: 编辑部门
     * @param vo
     * @return: int
     */
    int updateDept(UpdateDeptReqVO vo);

    /**
     * @description: 根据部门id删除部门
     * @param deptId
     * @return: int
     */
    int deleteDept(String deptId);
}
