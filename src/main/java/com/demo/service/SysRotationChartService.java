package com.demo.service;

import com.demo.pojo.SysRotationChart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.AddRotationChartReqAddVO;
import com.demo.vo.req.RotationPageReqVO;
import com.demo.vo.req.UpdateRotationChartReqVO;
import com.demo.vo.resp.PageInfoVO;

import java.util.List;

/**
 * <p>
 * 轮播图表结构 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-02-01
 */
public interface SysRotationChartService extends IService<SysRotationChart> {
    
    /**
     * @description: 分页查询轮播图信息
     * @param vo
     * @return: com.demo.vo.resp.PageInfoVO<com.demo.pojo.SysRotationChart>
     */
    PageInfoVO<SysRotationChart> pageInfo(RotationPageReqVO vo);

    /**
     * @description: 添加轮播图信息
     * @param vo
     * @param userId
     * @return: int
     */
    int addRotationChart(AddRotationChartReqAddVO vo, String userId);

    /**
     * @description: 编辑轮播图信息
     * @param vo
     * @param userId
     * @return: int
     */
    int updateRotationChart(UpdateRotationChartReqVO vo, String userId);

    /**
     * @description: 批量删除轮播图
     * @param ids
     * @return: int
     */
    int deleteRotationChartById(List<String> ids);

    /**
     * @description: 查询出所有的轮播图 
     * @return: java.util.List<com.demo.pojo.SysRotationChart>
     */
    List<SysRotationChart> selectAll();
}
