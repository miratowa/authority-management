package com.demo.service;

import com.demo.pojo.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.AddRoleVO;
import com.demo.vo.req.RolePageReqVO;
import com.demo.vo.req.UpdateRoleReqVO;
import com.demo.vo.resp.PageInfoVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
public interface SysRoleService extends IService<SysRole> {
    /**
     * @description: 分页查询角色信息
     * @param vo 封装的条件对象
     * @return: java.util.List<com.demo.pojo.SysRole>
     */
    PageInfoVO<SysRole> pageInfo(RolePageReqVO vo);

    /**
     * @description: 添加角色
     * @param vo    接受添加权限表单提交过来的数据
     * @return: int
     */
    SysRole addRole(AddRoleVO vo);

    /**
     * @description: 获取所有角色列表
     * @return: java.util.List<com.demo.pojo.SysRole>
     */
    List<SysRole> getAllRole();

    /**
     * @description: 获取角色详情信息
     * @param roleId
     * @return: com.demo.pojo.SysRole
     */
    SysRole detailInfo(String roleId);

    /**
     * @description: 编辑角色信息
     * @param vo
     * @return: int
     */
    int updateRoleByRoleId(UpdateRoleReqVO vo);

    /**
     * @description: 根据角色id删除指定角色
     * @param roleId
     * @return: int
     */
    int deleteRoleByRoleId(String roleId);

    /**
     * @description: 根据角色id获取角色名称
     * @return: java.util.List<java.lang.String>
     * @param roleIds
     */
    List<String> getRoleNamesByRoleIds(List<String> roleIds);
}
