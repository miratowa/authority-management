package com.demo.service;

import com.demo.pojo.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.FilePageReqVO;
import com.demo.vo.resp.PageInfoVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 文件信息存储表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-01-31
 */
public interface SysFileService extends IService<SysFile> {

    /**
     * @description: 文件上传方法
     * @param file
     * @param userId
     * @param typeId
     * @return: java.lang.String
     */
    String fileUpload(MultipartFile file, String userId, Integer typeId);

    /**
     * @description: 文件下载方法
     * @param fileId
     * @param resp
     * @return: void
     */
    void downloadFile(String fileId, HttpServletResponse resp);

    /**
     * @description: 批量删除文件
     * @param fileUrl
     * @return: int
     */
    int deleteFile(List<String> fileUrl);

    /**
     * @description: 分页查询当前用户的文件数据
     * @param vo
     * @param userId
     * @return: com.demo.vo.resp.PageInfoVO<com.demo.pojo.SysFile>
     */
    PageInfoVO<SysFile> pageInfo(FilePageReqVO vo, String userId);
}
