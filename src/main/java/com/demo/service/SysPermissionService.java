package com.demo.service;

import com.demo.pojo.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.AddPermissionReqVO;
import com.demo.vo.req.UpdatePermissionReqVO;
import com.demo.vo.resp.PermissionRespNodeVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
public interface SysPermissionService extends IService<SysPermission> {
    /**
     * @description: 查出所有菜单列表
     * @return: java.util.List<com.demo.pojo.SysPermission>
     */
    List<SysPermission> selectAll();

    /**
     * @description: 按照递归的方法查出所有菜单层级
     * @return: java.util.List<com.demo.vo.resp.PermissionRespNode>
     */
    List<PermissionRespNodeVO> selectAllMenuByTree();
    
    /**
     * @description: 添加菜单权限
     * @param vo
     * @return: int
     */
    int addPermission(AddPermissionReqVO vo);

    /**
     * @description: 编辑菜单权限
     * @param vo
     * @return: void
     */
    void updatePermission(UpdatePermissionReqVO vo);

    /**
     * @description: 删除菜单权限
     * @param permissionId
     * @return: void
     */
    void deletePermissionByPermissionId(String permissionId);
    /**
     * @description: 根据权限加载左边导航栏
     * @return: java.util.List<com.demo.vo.resp.PermissionRespNodeVO>
     */
    List<PermissionRespNodeVO> getPermissionTree(String userId);

    List<PermissionRespNodeVO> getPermissionAllTree();

    /**
     * @description: 根据用户id获取权限对象
     * @param userId
     * @return: java.util.List<com.demo.pojo.SysPermission>
     */
    List<SysPermission> getPermissionsByRoleId(String userId);
}
