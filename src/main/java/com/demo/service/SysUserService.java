package com.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.pojo.SysUser;
import com.demo.vo.req.*;
import com.demo.vo.resp.HomeRespVO;
import com.demo.vo.resp.LoginRespVO;
import com.demo.vo.resp.PageInfoVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-23
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * @description: 登录验证方法
     * @param vo    封装的登录数据
     * @return: com.demo.vo.resp.LoginRespVO
     */
    LoginRespVO login(LoginReqVO vo);

    /**
     * @description: 退出登录
     * @param accessToken   业务token
     * @param refreshToken  刷新token
     * @return: void
     */
    void logout(String accessToken, String refreshToken);

    /**
     * @description: 获取主页显示数据
     * @param userId
     * @return: com.demo.vo.resp.HomeRespVO
     */
    HomeRespVO getHomeInfo(String userId);

    /**
     * @description: 分页查询用户信息
     * @param vo    查询条件
     * @return: java.util.List<com.demo.pojo.SysUser>
     */
    PageInfoVO<SysUser> pageInfo(UserPageReqVO vo);

    /**
     * @description: 添加用户
     * @param vo
     * @param operationId
     * @return
     */
    SysUser addUser(AddUserReqVO vo,String operationId);

    /**
     * @description: 自动刷新token
     * @param token
     * @return: java.lang.String
     */
    String refreshToken(String token);

    /**
     * @description: 编辑用户
     * @param vo    前端表单提交过来的数据
     * @param operationId   执行操作的人
     * @return: void
     */
    void UpdateUser(UpdateUserReqVO vo, String operationId);

    /**
     * @description: 批量删除用户
     * @param userIds
     * @return: void
     */
    void DeleteUsers(List<String> userIds);

    /**
     * @description: 获取用户详情信息
     * @param userId
     * @return: com.demo.pojo.SysUser
     */
    SysUser detailInfo(String userId);

    /**
     * @description: 完善用户个人详情信息
     * @param vo
     * @return: int
     */
    int updateDetailInfo(String userId,UpdateUserDetailInfoReqVO vo);

    /**
     * @description: 修改用户登录密码,并将token加入黑名单
     * @param userId
     * @param accessToken
     * @param refreshToken
     * @param vo
     * @return: int
     */
    int updateUserPwd(String userId,String accessToken, String refreshToken, UpdateUserPwdReqVO vo);
}
