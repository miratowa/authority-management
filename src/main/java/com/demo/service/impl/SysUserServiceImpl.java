package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.mapper.SysDeptMapper;
import com.demo.mapper.SysPermissionMapper;
import com.demo.mapper.SysUserMapper;
import com.demo.pojo.SysDept;
import com.demo.pojo.SysPermission;
import com.demo.pojo.SysRolePermission;
import com.demo.pojo.SysUser;
import com.demo.service.*;
import com.demo.util.JWTUtil;
import com.demo.util.PasswordUtils;
import com.demo.util.RedisUtil;
import com.demo.vo.req.*;
import com.demo.vo.resp.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-23
 */
@Service
@Slf4j
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserRoleService userRoleService;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysRolePermissionService rolePermissionService;

    @Autowired
    private SysPermissionMapper permissionMapper;

    @Override
    public LoginRespVO login(LoginReqVO vo) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("username", vo.getUsername());
        SysUser user = sysUserMapper.selectOne(wrapper);
        if (user == null) {
            // 如果用户名为空，提示找不到用户
            throw new BusinessException(BaseResponseCode.ACCOUNT_ERROR);
        }
        if (user.getStatus() == 2) {
            // 检验账户是否被封禁
            throw new BusinessException(BaseResponseCode.ACCOUNT_LOCK_tip);
        }
        // 账号通过,验证密码
        if (!PasswordUtils.matches(user.getSalt(), vo.getPassword(), user.getPassword())) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_PASSWORD_ERROR);
        }
        // 创建需要携带的数据
        Map<String, Object> claims = new HashMap<>();
        claims.put(Constant.JWT_USER_NAME, user.getUsername());
        claims.put(Constant.ROLES_INFOS_KEY, getRoleByUserId(user.getId()));
        claims.put(Constant.PERMISSIONS_INFOS_KEY, getPermissionsByUserId(user.getId()));

        // 生成token
        String accessToken = JWTUtil.getAccessToken(user.getId(), claims);
        log.info("accessToken={}",accessToken);

        // 刷新时的token
        Map<String, Object> refreshClaims = new HashMap<>();
        refreshClaims.put(Constant.JWT_USER_NAME, user.getUsername());
        String refreshToken = null;

        // 判断登录来源1.Web,2.App
        if (vo.getType() == "1") {
            refreshToken = JWTUtil.getRefreshToken(user.getId(), refreshClaims);
        } else {
            refreshToken = JWTUtil.getRefreshAppToken(user.getId(), refreshClaims);
        }
        log.info("refreshToken={}", refreshToken);
        LoginRespVO loginRespVO = new LoginRespVO();
        loginRespVO.setAccessToken(accessToken);
        loginRespVO.setRefreshToken(refreshToken);
        loginRespVO.setUserId(user.getId());
        return loginRespVO;
    }

    @Override
    public void logout(String accessToken, String refreshToken) {
        if (StringUtils.isBlank(accessToken) || StringUtils.isBlank(refreshToken)) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            subject.logout();
        }
        String userId = JWTUtil.getUserId(accessToken);
        //  access_token 主动退出后加入黑名单 key
        redisUtil.set(Constant.JWT_ACCESS_TOKEN_BLACKLIST + accessToken, userId, JWTUtil.getRemainingTime(accessToken), TimeUnit.MILLISECONDS);
        //  refresh_token 主动退出后加入黑名单 key
        redisUtil.set(Constant.JWT_REFRESH_TOKEN_BLACKLIST + accessToken, userId, JWTUtil.getRemainingTime(refreshToken), TimeUnit.MILLISECONDS);

    }

    @Override
    public HomeRespVO getHomeInfo(String userId) {
        HomeRespVO vo=new HomeRespVO();
        vo.setMenus(permissionService.getPermissionTree(userId));
        // 查询用户，获取用户资料信息
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("id", userId);
        SysUser sysUser = sysUserMapper.selectOne(wrapper);
        UserInfoRespVO respVO = new UserInfoRespVO();
        if (sysUser != null) {
            BeanUtils.copyProperties(sysUser,respVO);
            respVO.setDeptName("xx公司");
        }
        vo.setUserInfo(respVO);
        return vo;
    }

    @Override
    public PageInfoVO<SysUser> pageInfo(UserPageReqVO vo) {
        // 创建分页条件器
        Page<SysUser> page = new Page<>(Integer.parseInt(vo.getPageNum()), Integer.parseInt(vo.getPageSize()));
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        // 判断是否有模糊查询条件
        if (StringUtils.isNotBlank(vo.getUserId())) {
            wrapper.like("id", vo.getUserId());
        }
        if (StringUtils.isNotBlank(vo.getUsername())) {
            wrapper.like("username", vo.getUsername());
        }
        if (StringUtils.isNotBlank(vo.getNickName())) {
            wrapper.like("nick_name", vo.getNickName());
        }
        if (vo.getStatus() != null) {
            wrapper.eq("status", vo.getStatus());
        }
        if (StringUtils.isNotBlank(vo.getStartTime()) && StringUtils.isNotBlank(vo.getEndTime())) {
            wrapper.between("create_time", vo.getStartTime(), vo.getEndTime());
        }
        // 分页查询出角色数据
        Page<SysUser> userPage = sysUserMapper.selectPage(page, wrapper);
        // 封装返回数据
        PageInfoVO<SysUser> result = new PageInfoVO<>();
        result.setTotalRows(userPage.getTotal());
        result.setPageSize((int) userPage.getSize());
        result.setPageNum((int) userPage.getCurrent());
        result.setTotalPages((int) (userPage.getTotal()/userPage.getSize()));
        result.setList(userPage.getRecords());

        return result;
    }

    @Override
    public SysUser addUser(AddUserReqVO vo, String operationId) {
        QueryWrapper<SysDept> wrapper = new QueryWrapper<>();
        wrapper.eq("id", vo.getDeptId());
        SysUser user = new SysUser();
        BeanUtils.copyProperties(vo,user);
        user.setCreateId(operationId);
        user.setId(UUID.randomUUID().toString());
        // 查出所属公司名称
        SysDept dept=deptMapper.selectOne(wrapper);
        if (dept == null) {
            throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_BTN_ERROR);
        }else{
            user.setDeptName(dept.getName());
        }
        // 密码需要加密
        String salt=PasswordUtils.getSalt();
        String ecdPwd=PasswordUtils.encode(vo.getPassword(),salt);
        user.setSalt(salt);
        user.setPassword(ecdPwd);
        // 添加用户到数据库
        int insert = sysUserMapper.insert(user);
        if (insert != 1) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        return user;
    }

    @Override
    public String refreshToken(String token) {
        // 判断token是否合规
        if (!JWTUtil.validateToken(token)||redisUtil.hasKey(Constant.JWT_REFRESH_TOKEN_BLACKLIST+token)) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        String userId = JWTUtil.getUserId(token);
        String userName = JWTUtil.getUserName(token);
        Map claims = new HashMap();
        claims.put(Constant.JWT_USER_NAME, userName);
        claims.put(Constant.ROLES_INFOS_KEY, getRoleByUserId(userId));
        claims.put(Constant.PERMISSIONS_INFOS_KEY, getPermissionsByUserId(userId));
        // 生成新的token
        String accessToken = JWTUtil.getAccessToken(userId, claims);
        return accessToken;
    }

    @Override
    public void UpdateUser(UpdateUserReqVO vo,String operationId) {
        SysUser user = new SysUser();
        BeanUtils.copyProperties(vo, user);
        user.setUpdateId(operationId);
        // 查询所属公司名称
        QueryWrapper<SysDept> deptWrapper = new QueryWrapper<>();
        deptWrapper.eq("id", vo.getDeptId());
        SysDept dept=deptMapper.selectOne(deptWrapper);
        if (dept == null) {
            throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_BTN_ERROR);
        }else{
            user.setDeptName(dept.getName());
        }

        SysUser sysUser = sysUserMapper.selectById(vo.getId());
        if (sysUser == null) {
            // 传入的id为空
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        if (StringUtils.isNotBlank(vo.getPassword()) && !vo.getPassword().equals("")) {
            // 密码入库时需要加密
            String salt = PasswordUtils.getSalt();
            user.setSalt(salt);
            user.setPassword(PasswordUtils.encode(vo.getPassword(), salt));
        } else {
            // 传入的密码为空
            throw new BusinessException(BaseResponseCode.PASSWORD_ERROR);
        }
        // 判断用户是否被禁用
        if (vo.getStatus() == 2) {
            // 用户被禁用,标记进入redis
            redisUtil.set(Constant.ACCOUNT_LOCK_KEY + vo.getId(), vo.getId());
        } else {
            // 用户被取消禁用
            redisUtil.del(Constant.ACCOUNT_LOCK_KEY+vo.getId());
        }

        QueryWrapper<SysUser> wrapper = new QueryWrapper<SysUser>();
        wrapper.eq("id", vo.getId());
        int update = sysUserMapper.update(user,wrapper);
        if (update < 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
    }

    @Override
    public void DeleteUsers(List<String> userIds) {
        if (!userIds.isEmpty()) {
            for (String userId : userIds) {
                sysUserMapper.delete(new QueryWrapper<SysUser>().eq("id", userId));
                // 删除成功后需要标记用户
                redisUtil.set(Constant.DELETED_USER_KEY + userId, userId);
                // 权限发生改变后,需要删除用户之前的缓存授权信息
                redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userId);
            }
        }
    }

    @Override
    public SysUser detailInfo(String userId) {
        return sysUserMapper.selectById(userId);
    }

    @Override
    public int updateDetailInfo(String userId,UpdateUserDetailInfoReqVO vo) {
        if (sysUserMapper.selectById(userId) == null) {
            // 传入的用户id错误
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        UpdateWrapper<SysUser> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", userId);
        wrapper.set("update_id", userId);
        SysUser user = new SysUser();
        BeanUtils.copyProperties(vo, user);
        int update = sysUserMapper.update(user, wrapper);
        if (update != 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return update;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateUserPwd(String userId,String accessToken, String refreshToken, UpdateUserPwdReqVO vo) {
        SysUser user = sysUserMapper.selectById(userId);
        if (user == null) {
            // 传入的用户id错误
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 验证旧密码是否相同
        if (!PasswordUtils.matches(user.getSalt(), vo.getOldPwd(), user.getPassword())) {
            throw new BusinessException(BaseResponseCode.OLD_PASSWORD_ERROR);
        }
        // 执行修改密码的操作
        user.setPassword(PasswordUtils.encode(vo.getNewPwd(),user.getSalt()));
        int update = sysUserMapper.update(user, new UpdateWrapper<SysUser>().eq("id", userId));
        if (update != 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        // 密码修改成功后,不允许再用之前的token登录,需要将accessToken和refreshToken加入黑名单
        redisUtil.set(Constant.JWT_ACCESS_TOKEN_BLACKLIST + accessToken, userId, JWTUtil.getRemainingTime(accessToken), TimeUnit.MILLISECONDS);
        redisUtil.set(Constant.JWT_REFRESH_TOKEN_BLACKLIST + accessToken, userId, JWTUtil.getRemainingTime(refreshToken), TimeUnit.MILLISECONDS);
        // 权限发生改变后,需要删除用户之前的缓存授权信息
        redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userId);
        return update;
    }

    /**
     * @param userId
     * @description: 根据用户id查询用户的身份
     * @return: java.util.List<java.lang.String>
     */
    public List<String> getRoleByUserId(String userId) {
        List<String> roleIds = userRoleService.getRolesByUserId(userId);
        List<String> roleNames = roleService.getRoleNamesByRoleIds(roleIds);
        if (!roleNames.isEmpty()) {
            return roleNames;
        }
        return null;
    }

    /**
     * @description: 通过用户ID获取权限
     * @param userId
     * @return: java.util.List<java.lang.String>
     */
    public List<String> getPermissionsByUserId(String userId) {
        List<String> roleIds = userRoleService.getRolesByUserId(userId);
        List<String> result = new ArrayList<>();
        List<String> perms = new ArrayList<>();
        if (!roleIds.isEmpty()) {
            for (String roleId : roleIds) {
                // 根据角色id获取角色拥有的权限id
                List<String> permissionIds = rolePermissionService.getPermissionIdsByRoleId(roleId);
                for (String permissionId : permissionIds) {
                    result.add(permissionId);
                }
            }
        }
        // 根据权限id获取到具体的权限
        if (!result.isEmpty()) {
            for (String s : result) {
                List<SysPermission> permissions = permissionMapper.selectList(new QueryWrapper<SysPermission>().eq("id", s).eq("status",1).orderByDesc("order_num"));
                for (SysPermission permission : permissions) {
                    if (permission.getPerms() != null && !permission.getPerms().equals("")) {
                        perms.add(permission.getPerms());
                    }
                }
            }
        }
        return perms;
    }
}
