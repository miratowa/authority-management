package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.pojo.SysRolePermission;
import com.demo.mapper.SysRolePermissionMapper;
import com.demo.service.SysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.vo.req.RolePermissionOperationReqVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements SysRolePermissionService {
    @Autowired
    private SysRolePermissionMapper rolePermissionMapper;

    @Override
    public void addRolePermission(RolePermissionOperationReqVO vo) {
        // 首先需要删除角色之前所拥有的权限
        SysRolePermission rolePermission = new SysRolePermission();
        vo.getRoleId();
        deletePermissionByRoleId(vo.getRoleId());
        for (String permissionId : vo.getPermissionIds()) {
            rolePermission.setId(UUID.randomUUID().toString());
            rolePermission.setRoleId(vo.getRoleId());
            rolePermission.setPermissionId(permissionId);
            rolePermissionMapper.insert(rolePermission);
        }

    }

    @Override
    public List<String> getPermissionIdsByRoleId(String roleId) {
        QueryWrapper<SysRolePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        List<SysRolePermission> rolePermissions = rolePermissionMapper.selectList(wrapper);
        List<String> permissionIds = new ArrayList<>();
        for (SysRolePermission rolePermission : rolePermissions) {
            permissionIds.add(rolePermission.getPermissionId());
        }
        return permissionIds;
    }

    @Override
    public int deletePermissionByRoleId(String roleId) {
        QueryWrapper<SysRolePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        int delete = rolePermissionMapper.delete(wrapper);
        return delete;
    }
}
