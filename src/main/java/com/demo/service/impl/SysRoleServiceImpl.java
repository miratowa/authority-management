package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.pojo.SysRole;
import com.demo.mapper.SysRoleMapper;
import com.demo.service.SysPermissionService;
import com.demo.service.SysRolePermissionService;
import com.demo.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.service.SysUserRoleService;
import com.demo.util.RedisUtil;
import com.demo.util.TokenSetting;
import com.demo.vo.req.AddRoleVO;
import com.demo.vo.req.RolePageReqVO;
import com.demo.vo.req.RolePermissionOperationReqVO;
import com.demo.vo.req.UpdateRoleReqVO;
import com.demo.vo.resp.PageInfoVO;
import com.demo.vo.resp.PermissionRespNodeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysRolePermissionService rolePermissionService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private SysUserRoleService userRoleService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TokenSetting tokenSetting;

    @Override
    public PageInfoVO<SysRole> pageInfo(RolePageReqVO vo) {
        Page<SysRole> page = new Page<>(Integer.parseInt(vo.getPageNum()), Integer.parseInt(vo.getPageSize()));
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        // 判断是否有模糊查询条件
        if (StringUtils.isNotBlank(vo.getRoleId())) {
            wrapper.like("id", vo.getRoleId());
        }
        if (StringUtils.isNotBlank(vo.getRoleName())) {
            wrapper.like("name", vo.getRoleName());
        }
        if (vo.getStatus() != null) {
            wrapper.eq("status", vo.getStatus());
        }
        if (StringUtils.isNotBlank(vo.getStartTime()) && StringUtils.isNotBlank(vo.getEndTime())) {
            wrapper.between("create_time", vo.getStartTime(), vo.getEndTime());
        }
        // 分页查询出符合条件的数据
        Page<SysRole> sysRolePage = roleMapper.selectPage(page, wrapper);

        // 封装返回数据
        PageInfoVO<SysRole> result = new PageInfoVO<>();
            result.setTotalRows(sysRolePage.getTotal());
            result.setTotalPages((int) (sysRolePage.getTotal()/ sysRolePage.getSize()));
            result.setPageNum((int) sysRolePage.getCurrent());
            result.setPageSize((int) sysRolePage.getSize());
            result.setList(sysRolePage.getRecords());

        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysRole addRole(AddRoleVO vo) {
        SysRole role = new SysRole();
        BeanUtils.copyProperties(vo,role);
        role.setId(UUID.randomUUID().toString());
        int insert = roleMapper.insert(role);
        if (insert != 1) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 判断是否添加了权限,如果有就执行添加权限的操作
        if (vo.getPermissionIds() != null || !vo.getPermissionIds().isEmpty()) {
            RolePermissionOperationReqVO operationReqVO = new RolePermissionOperationReqVO();
            operationReqVO.setRoleId(role.getId());
            operationReqVO.setPermissionIds(vo.getPermissionIds());
            // 批量添加角色权限
            rolePermissionService.addRolePermission(operationReqVO);
        }
        return role;
    }

    @Override
    public List<SysRole> getAllRole() {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        List<SysRole> roles = roleMapper.selectList(wrapper);
        return roles;
    }

    @Override
    public SysRole detailInfo(String roleId) {
        SysRole sysRole = roleMapper.selectOne(new QueryWrapper<SysRole>().eq("id", roleId));
        if (sysRole == null) {
            log.error("传入的角色id不合法");
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 获取所有的菜单权限树
        List<PermissionRespNodeVO> permissionAllTree = permissionService.getPermissionAllTree();
        // 获取角色所拥有的权限
        List<String> permissionIds = rolePermissionService.getPermissionIdsByRoleId(roleId);
        Set<String> checkList=new HashSet<>(permissionIds);
        setChecked(permissionAllTree,checkList);
        sysRole.setPermissionRespNode(permissionAllTree);
        return sysRole;
    }

    /**
     * @description: 用于回显角色已经拥有过的权限
     * @param permissionAllTree 所有权限
     * @param checkList         角色已拥有了的权限
     * @return: void
     */
    private void setChecked(List<PermissionRespNodeVO> permissionAllTree, Set<String> checkList) {
        for (PermissionRespNodeVO node : permissionAllTree) {
            if (checkList.contains(node.getId()) && (node.getChildren() == null || node.getChildren().isEmpty())) {
                node.setChecked(true);
            }
            setChecked((List<PermissionRespNodeVO>) node.getChildren(),checkList);
        }
    }

    @Override
    public int updateRoleByRoleId(UpdateRoleReqVO vo) {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        wrapper.eq("id", vo.getId());
        SysRole sysRole = roleMapper.selectOne(wrapper);
        if (sysRole == null) {
            // 传入的角色id错误
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 保存数据到数据库
        SysRole role = new SysRole();
        BeanUtils.copyProperties(vo,role);
        int update = roleMapper.updateById(role);
        if (update != 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        // 把角色和菜单权限关联数据修改
        RolePermissionOperationReqVO reqVO = new RolePermissionOperationReqVO();
        reqVO.setRoleId(vo.getId());
        reqVO.setPermissionIds(vo.getPermissionIds());
        rolePermissionService.addRolePermission(reqVO);
        // 把该角色关联的用户标记起来，需要去刷新jwt token
        // 通过角色id查到用户
        List<String> userIds = userRoleService.getUserIdsByRoleId(vo.getId());
        if (!userIds.isEmpty()) {
            for (String userId : userIds) {
                // 标记起来,需要主动刷新token
                redisUtil.set(Constant.JWT_REFRESH_KEY + userId, userId, tokenSetting.getAccessTokenExpireTime().toMillis(), TimeUnit.MILLISECONDS);
                // 权限发生改变后,需要删除用户之前的缓存授权信息
                redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userId);
            }
        }
        return update;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRoleByRoleId(String roleId) {
        // 逻辑删除用户
        int delete = roleMapper.deleteById(roleId);
        if (delete != 1) {
            // 传入的角色id有误
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 角色菜单权限关联数据删除
        rolePermissionService.deletePermissionByRoleId(roleId);
        // 查找出所有拥有该角色的用户
        List<String> userIds = userRoleService.getUserIdsByRoleId(roleId);
        // 角色用户关联数据删除
        userRoleService.deleteUserByRoleId(roleId);
        /**
         * 刪除角色后 要主动去刷新跟該角色有关联用户的token
         * 因为用户所拥有的菜单权限是通过角色去关联的
         * 所以要把跟这个角色关联的用户 都要重新刷新token
         */
        if (!userIds.isEmpty()) {
            for (String userId : userIds) {
                redisUtil.set(Constant.JWT_REFRESH_KEY + userId, userId, tokenSetting.getAccessTokenExpireTime().toMillis(), TimeUnit.MILLISECONDS);
                // 权限发生改变后,需要删除用户之前的缓存授权信息
                redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userId);
            }
        }
        return delete;
    }

    @Override
    public List<String> getRoleNamesByRoleIds(List<String> roleIds) {
        List<SysRole> roles = new ArrayList<>();
        if (!roleIds.isEmpty()) {
            for (String roleId : roleIds) {
                roles.add(roleMapper.selectById(roleId));
            }
        }
        List<String> roleNames = new ArrayList<>();
        if (!roles.isEmpty()) {
            for (SysRole role : roles) {
                roleNames.add(role.getName());
            }
        }
        return roleNames;
    }
}
