package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.pojo.SysFile;
import com.demo.mapper.SysFileMapper;
import com.demo.service.SysFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.vo.req.FilePageReqVO;
import com.demo.vo.resp.PageInfoVO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 文件信息存储表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-01-31
 */
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {

    @Value("${file.path}")
    private String FILE_PATH;

    @Value("${file.base-url}")
    private String fileBaseUrl;

    @Autowired
    private SysFileMapper fileMapper;

    @Override
    public String fileUpload(MultipartFile file, String userId, Integer typeId) {
        String originalFilename = file.getOriginalFilename();
        String extensionName = originalFilename.substring(originalFilename.lastIndexOf(".") + 1).toLowerCase();
        String fileId = UUID.randomUUID().toString();
        String fileName = fileId + "." + extensionName;
        File destFile = new File(FILE_PATH + fileName);
        // 判断是否包含这个文件夹
        if (!destFile.getParentFile().exists()) {
            // 如果没有就创建文件夹
            destFile.getParentFile().mkdirs();
        }
        String fileUrl = fileBaseUrl + fileName;
        try {
            file.transferTo(destFile);
            SysFile sysFile = new SysFile();
            sysFile.setCreateId(userId);
            sysFile.setExtensionName(extensionName);
            sysFile.setFileName(fileName);
            sysFile.setOriginalName(originalFilename);
            sysFile.setSize(FileUtils.byteCountToDisplaySize(file.getSize()));
            sysFile.setType(typeId);
            sysFile.setFileUrl(fileUrl);
            // 执行添加操作
            int insert = fileMapper.insert(sysFile);
            if (insert < 0) {
                throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(BaseResponseCode.UPLOAD_FILE_ERROR);
        }
        return fileUrl;
    }

    @Override
    public void downloadFile(String fileId, HttpServletResponse resp) {
        SysFile sysFile = fileMapper.selectById(fileId);
        if (sysFile == null) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        ServletOutputStream sos = null;
        try {
            resp.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            String fileName = new String(sysFile.getOriginalName().getBytes("UTF-8"), "ISO-8859-1");
            resp.setHeader("content-disposition", String.format("attachment:filename=%s", fileName));
            File file = new File(FILE_PATH + sysFile.getFileName());
            sos = resp.getOutputStream();
            IOUtils.write(FileUtils.readFileToString(file), sos);
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        } catch (IOException e) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        } finally {
            if (sos != null) {
                try {
                    sos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int deleteFile(List<String> fileUrl) {
        int delete = 0;
        for (String s : fileUrl) {
            delete  = fileMapper.delete(new QueryWrapper<SysFile>().eq("file_url", s));
            // 还需删除磁盘中的文件
            String fileName = s.substring(s.lastIndexOf("/")+1);
            deleteDestFile(fileName);
        }
        if (delete < 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return delete;
    }

    private void deleteDestFile(String fileName) {
        File file = new File(FILE_PATH + fileName);
        if (file.exists()) {
            file.delete();
        }
    }

    @Override
    public PageInfoVO<SysFile> pageInfo(FilePageReqVO vo, String userId) {
        SysFile sysFile = new SysFile();
        BeanUtils.copyProperties(vo, sysFile);
        QueryWrapper<SysFile> wrapper = new QueryWrapper<>();
        wrapper.eq("create_id", userId);
        Page<SysFile> page = new Page<SysFile>(vo.getPageNum(), vo.getPageSize());
        Page<SysFile> pageInfo = fileMapper.selectPage(page, wrapper);

        // 封装返回数据
        PageInfoVO<SysFile> result = new PageInfoVO<>();
        result.setPageNum((int) pageInfo.getCurrent());
        result.setPageSize((int) pageInfo.getSize());
        result.setTotalRows(pageInfo.getTotal());
        // 总页数
        int totalPages = (int) (pageInfo.getTotal() / pageInfo.getSize());
        result.setTotalPages(totalPages);
        result.setList(pageInfo.getRecords());
        return result;
    }
}
