package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.mapper.SysUserMapper;
import com.demo.pojo.SysDept;
import com.demo.mapper.SysDeptMapper;
import com.demo.pojo.SysUser;
import com.demo.service.SysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.util.CodeUtil;
import com.demo.util.RedisUtil;
import com.demo.vo.req.AddDeptReqVO;
import com.demo.vo.req.UpdateDeptReqVO;
import com.demo.vo.resp.DeptRespNodeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Service
@Slf4j
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserMapper userMapper;
    @Override
    public List<SysDept> selectAll() {
        QueryWrapper<SysDept> wrapper = new QueryWrapper<>();
        // 查询出所有部门信息
        List<SysDept> depts = deptMapper.selectList(wrapper);
        for (SysDept dept : depts) {
            QueryWrapper<SysDept> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("id", dept.getPid());
            // 查询出父级部门
            SysDept parent = deptMapper.selectOne(wrapper1);
            if (parent != null) {
                // 获取上级部门的名称
                dept.setPidName(parent.getName());
            }
        }
        return depts;
    }

    @Override
    public List<DeptRespNodeVO> getDeptTree(String deptId) {
        // 创建集合用于返回数据
        List<DeptRespNodeVO> result = new ArrayList<>();
        // 查出所有部门信息
        List<SysDept> all = deptMapper.selectList(null);
        // 因为考虑到编辑部门信息时,需要排除自身的子集节点
        if (!StringUtils.isEmpty(deptId) && !all.isEmpty()) {
            for (SysDept sysDept : all) {
                if (sysDept.getId().equals(deptId)) {
                    all.remove(sysDept);
                    break;
                }
            }
        }
        // 如果没有部门,就设置一个默认值
        DeptRespNodeVO vo = new DeptRespNodeVO();
        vo.setId("0");
        vo.setTitle("默认顶级部门");
        vo.setChildren(getTree(all));
        result.add(vo);
        return result;
    }

    @Override
    public SysDept addDept(AddDeptReqVO vo) {
        // 为了维护更深层级关系(规则：父级关系编码+自己的编码)
        String relationCode;
        long result = redisUtil.incr(Constant.DEPT_CODE_KEY, 1);
        // 自动补齐部门编码
        String deptNo = CodeUtil.deptCode(String.valueOf(result), 7, "0");
        // 查出是否有父级部门
        QueryWrapper<SysDept> wrapper = new QueryWrapper<>();
        wrapper.eq("id", vo.getPid());
        SysDept parentDept = deptMapper.selectOne(wrapper);
        if (vo.getPid().equals("0")) {
            // 没有父级部门,则父级部门编号就是自己的部门编号
            relationCode = deptNo;
        } else if (parentDept == null) {
            // 查询有错,传入的pid有误
            log.info("传入的pid:{},不合法");
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        } else {
            // 有父级编码，部门编号为：父级关系编码+自己的部门编号
            relationCode = parentDept.getRelationCode() + deptNo;
        }
        SysDept dept = new SysDept();
        BeanUtils.copyProperties(vo, dept);
        dept.setId(UUID.randomUUID().toString());
        dept.setDeptNo(deptNo);
        dept.setRelationCode(relationCode);

        // 添加到数据库中
        int insert = deptMapper.insert(dept);
        if (insert != 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return dept;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDept(UpdateDeptReqVO vo) {
        SysDept sysDept = deptMapper.selectById(vo.getId());
        if (sysDept == null) {
            // 传入的部门id错误
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        SysDept dept = new SysDept();
        BeanUtils.copyProperties(vo, dept);
        int update = deptMapper.updateById(dept);
        if (update != 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        // 判断层级关系是否发生变化（新的父级id是否等于原先的父级id）
        if (!sysDept.getPid().equals(vo.getPid())) {
            // 获取到新的父级部门
            SysDept newParent = deptMapper.selectById(vo.getPid());
            if (newParent == null && !newParent.getPid().equals("0")) {
                // 传入的pid错误
                throw new BusinessException(BaseResponseCode.DATA_ERROR);
            }
            // 新的层级关系编码
            String newRelationCode;
            // 为了维护更深层级关系(规则：父级关系编码+自己的编码)
            // 如果是根目录降到其他目录下
            if (sysDept.getPid().equals("0")) {
                newRelationCode = newParent.getRelationCode() + sysDept.getDeptNo();
                // 如果是其他目录升级到了根目录下
            } else if (vo.getPid().equals("0")) {
                newRelationCode = sysDept.getDeptNo();
            } else {
                newRelationCode = newParent.getRelationCode()+sysDept.getDeptNo();
            }
            UpdateWrapper<SysDept> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("id", dept.getId());
            updateWrapper.like("relation_code", sysDept.getRelationCode());
            updateWrapper.set("relation_code", newRelationCode);
            deptMapper.update(dept, updateWrapper);
        }
        return update;
    }

    @Override
    public int deleteDept(String deptId) {
        SysDept sysDept = deptMapper.selectById(deptId);
        if (sysDept == null) {
            // 传入的部门id有误
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 查询出所有叶子结点
        QueryWrapper<SysDept> wrapper = new QueryWrapper<>();
        wrapper.like("relation_code", sysDept.getRelationCode());
        List<SysDept> dept = deptMapper.selectList(wrapper);
        // 根据部门id查询出所有与之关联的用户
        for (SysDept d : dept) {
            List<SysUser> userIds = userMapper.selectList(new QueryWrapper<SysUser>().eq("dept_id",d.getId()));
            if (!userIds.isEmpty()) {
                // 该部门下关联着角色,无法删除
                throw new BusinessException(BaseResponseCode.NOT_PERMISSION_DELETED_DEPT);
            }
        }
        // 执行逻辑删除操作
        int delete = 0;
        for (SysDept sysDept1 : dept) {
            // 需要删除所有叶子结点
             delete= deptMapper.deleteById(sysDept1.getId());
            if (delete < 1) {
                throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
            }
        }
        return delete;
    }

    /**
     * @description: 获取树形结构
     * @param all   所有部门信息集合
     * @return: java.util.List<com.demo.vo.resp.DeptRespNodeVO>
     */
    private List<DeptRespNodeVO> getTree(List<SysDept> all) {
        List<DeptRespNodeVO> list=new ArrayList<>();
        if (all.isEmpty()) {
            return list;
        }
        for (SysDept dept : all) {
            // 如果是最顶层,就遍历子集节点
            if (dept.getPid().equals("0")) {
                DeptRespNodeVO vo = new DeptRespNodeVO();
                vo.setId(dept.getId());
                vo.setTitle(dept.getName());
                vo.setChildren(getChildren(all,dept.getId()));
                list.add(vo);
            }
        }
        return list;
    }

    /**
     * @description: 根据id遍历出所有子集子节点
     * @param all
     * @param id
     * @return: java.util.List<com.demo.vo.resp.DeptRespNodeVO>
     */
    private List<DeptRespNodeVO> getChildren(List<SysDept> all, String id) {
        List<DeptRespNodeVO> list=new ArrayList<>();
        if (all.isEmpty()) {
            return list;
        }
        for (SysDept dept : all) {
            // 如果有父级部门,就遍历子集节点
            if (dept.getPid().equals(id)) {
                DeptRespNodeVO vo = new DeptRespNodeVO();
                vo.setId(dept.getId());
                vo.setTitle(dept.getName());
                vo.setChildren(getChildren(all,dept.getId()));
                list.add(vo);
            }
        }
        return list;
    }
}
