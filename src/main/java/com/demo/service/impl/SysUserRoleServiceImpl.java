package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.mapper.SysRoleMapper;
import com.demo.mapper.SysUserMapper;
import com.demo.pojo.SysRole;
import com.demo.pojo.SysUserRole;
import com.demo.mapper.SysUserRoleMapper;
import com.demo.service.SysRolePermissionService;
import com.demo.service.SysRoleService;
import com.demo.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.service.SysUserService;
import com.demo.util.RedisUtil;
import com.demo.util.TokenSetting;
import com.demo.vo.req.AssignUserRolesReqVO;
import com.demo.vo.resp.UserOwnRoleRespVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysRolePermissionService rolePermissionService;

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TokenSetting tokenSetting;

    @Override
    public UserOwnRoleRespVO getUserOwnRole(String userId) {
        UserOwnRoleRespVO vo = new UserOwnRoleRespVO();
        // 获取所有的角色
        vo.setAllRole(roleService.getAllRole());
        // 获取用户拥有的角色
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<SysUserRole> userRoles = userRoleMapper.selectList(wrapper);
        List<String> rolesList = new ArrayList<>();
        for (SysUserRole userRole : userRoles) {
            rolesList.add(userRole.getRoleId());
        }
        vo.setOwnRoles(rolesList);
        return vo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void AssignUserRoles(AssignUserRolesReqVO vo) {
        // 先删除用户原有的角色
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", vo.getUserId());
        userRoleMapper.delete(wrapper);
        // 角色发生改变,标记用户,要求主动刷新token
        redisUtil.set(Constant.JWT_REFRESH_KEY+vo.getUserId(),vo.getUserId(),tokenSetting.getAccessTokenExpireTime().toMillis(),TimeUnit.MILLISECONDS);
        if (vo.getRoleIds() == null || vo.getRoleIds().isEmpty()) {
            return;
        }
        int insert=0;
        for (String roleIds : vo.getRoleIds()) {
            SysUserRole userRole = new SysUserRole();
            userRole.setId(UUID.randomUUID().toString());
            userRole.setUserId(vo.getUserId());
            userRole.setRoleId(roleIds);
            // 批量赋予用户角色操作
            insert= userRoleMapper.insert(userRole);
            // 权限发生改变后,需要删除用户之前的缓存授权信息
            redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userRole.getUserId());
        }
        if (insert == 0) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
    }

    @Override
    public List<String> getUserIdsByRoleId(String roleId) {
        QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        List<SysUserRole> userRoles = userRoleMapper.selectList(wrapper);
        List<String> userIds = new ArrayList<>();
        for (SysUserRole userRole : userRoles) {
            userIds.add(userRole.getUserId());
        }
        return userIds;
    }

    @Override
    public int deleteUserByRoleId(String roleId) {
        int delete = userRoleMapper.deleteById(roleId);
        return delete;
    }

    @Override
    public List<String> getRolesByUserId(String userId) {
        List<String> roleIds = new ArrayList<>();
        List<SysUserRole> roles = userRoleMapper.selectList(new QueryWrapper<SysUserRole>().eq("user_id", userId));
        if (!roles.isEmpty()) {
            for (SysUserRole role : roles) {
                roleIds.add(role.getRoleId());
            }
        }
        return roleIds;
    }
}
