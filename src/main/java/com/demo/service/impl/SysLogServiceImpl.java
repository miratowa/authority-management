package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.pojo.SysLog;
import com.demo.mapper.SysLogMapper;
import com.demo.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.vo.req.LogPageReqVO;
import com.demo.vo.resp.PageInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统日志 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {
    @Autowired
    private SysLogMapper logMapper;

    @Override
    public PageInfoVO<SysLog> pageInfo(LogPageReqVO vo) {
        Page<SysLog> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        QueryWrapper<SysLog> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(vo.getUserId())) {
            wrapper.like("user_id", vo.getUserId());
        }
        if (StringUtils.isNotBlank(vo.getUsername())) {
            wrapper.like("username", vo.getUsername());
        }
        if (StringUtils.isNotBlank(vo.getOperation())) {
            wrapper.like("operation", vo.getOperation());
        }
        if (StringUtils.isNotBlank(vo.getStartTime()) && StringUtils.isNotBlank(vo.getEndTime())) {
            wrapper.between("create_time", vo.getStartTime(), vo.getEndTime());
        }
        // 分页查询数据
        Page<SysLog> log = logMapper.selectPage(page, wrapper);
        // 封装返回数据
        PageInfoVO<SysLog> result = new PageInfoVO<>();
        // 当前页
        result.setPageNum((int) log.getCurrent());
        // 每页显示的数据
        result.setPageSize((int) log.getSize());
        // 总页数
        int totalPages = (int) (log.getTotal() / log.getSize());
        result.setTotalPages(totalPages);
        // 总记录数
        result.setTotalRows(log.getTotal());
        // 分页查询出的数据
        result.setList(log.getRecords());
        return result;
    }

    @Override
    public int deleteLogsByLogIds(List<String> logIds) {
        int delete = 0;
        if (!logIds.isEmpty()) {
            for (String logId : logIds) {
                delete=logMapper.deleteById(logId);
                if (delete < 1) {
                    throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
                }
            }
        }
        return delete;
    }
}
