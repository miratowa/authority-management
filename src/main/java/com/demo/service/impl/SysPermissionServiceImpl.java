package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.mapper.PermissionMapper;
import com.demo.mapper.SysRolePermissionMapper;
import com.demo.mapper.SysUserRoleMapper;
import com.demo.pojo.SysPermission;
import com.demo.mapper.SysPermissionMapper;
import com.demo.pojo.SysRolePermission;
import com.demo.pojo.SysUserRole;
import com.demo.service.SysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.service.SysRolePermissionService;
import com.demo.service.SysUserRoleService;
import com.demo.util.RedisUtil;
import com.demo.util.TokenSetting;
import com.demo.vo.req.AddPermissionReqVO;
import com.demo.vo.req.UpdatePermissionReqVO;
import com.demo.vo.resp.PermissionRespNodeVO;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.demo.exception.code.BaseResponseCode.OPERATION_MENU_PERMISSION_CATALOG_ERROR;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService {
    @Autowired
    private PermissionMapper permissionMapperII;

    @Autowired
    private SysPermissionMapper permissionMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysRolePermissionMapper rolePermissionMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private TokenSetting tokenSetting;

    @Autowired
    private SysUserRoleService userRoleService;

    @Autowired
    private SysRolePermissionService rolePermissionService;

    @Override
    public List<SysPermission> selectAll() {
        QueryWrapper<SysPermission> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("order_num");
        // 查询出所有菜单权限信息
        List<SysPermission> sysPermissions = permissionMapper.selectList(wrapper);
        if (!sysPermissions.isEmpty()) {
            for (SysPermission sysPermission : sysPermissions) {
                QueryWrapper<SysPermission> wrapper1 = new QueryWrapper<>();
                wrapper1.eq("id", sysPermission.getPid());
                SysPermission parent = permissionMapper.selectOne(wrapper1);
                if (parent != null) {
                    sysPermission.setPidName(parent.getName());
                }
            }
        }
        return sysPermissions;
    }

    @Override
    public List<PermissionRespNodeVO> selectAllMenuByTree() {
        QueryWrapper<SysPermission> wrapper = new QueryWrapper<>();
        // 先查询出所有的数据
        List<SysPermission> sysPermissions = permissionMapper.selectList(wrapper);
        List<PermissionRespNodeVO> results = new ArrayList<>();
        // 如果没有菜单,就设置一个默认值
        PermissionRespNodeVO vo = new PermissionRespNodeVO();
        vo.setId("0");
        vo.setTitle("默认顶级菜单");
        // 递归查询出所有的子集集合
        vo.setChildren(getTree(sysPermissions,false));
        results.add(vo);
        return results;
    }


    @Override
    public List<PermissionRespNodeVO> getPermissionTree(String userId) {
        return getTree(getPermissionsByRoleId(userId),false);
    }

    @Override
    public List<PermissionRespNodeVO> getPermissionAllTree() {
        return getTree(selectAll(),true);
    }

    @Override
    public int addPermission(AddPermissionReqVO vo) {
        SysPermission permission = new SysPermission();
        BeanUtils.copyProperties(vo,permission);
        // 进行数据的验证
        verifyForm(permission);
        // 执行添加操作
        permission.setId(UUID.randomUUID().toString());
        int count = permissionMapper.insert(permission);
        if (count != 1) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        return count;
    }

    @Override
    public void updatePermission(UpdatePermissionReqVO vo) {
        SysPermission permission = new SysPermission();
        BeanUtils.copyProperties(vo,permission);
        verifyForm(permission);
        // 判断要编辑的菜单是否存在
        SysPermission sysPermission = permissionMapper.selectById(vo.getId());
        if (sysPermission == null) {
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        // 所属菜单如果要发生变化，需要先判断当前是否存在子集节点
        if (!getChildren(sysPermission.getId(),selectAll()).isEmpty()) {
            throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_UPDATE);
        }

        // 执行修改操作
        int update = permissionMapper.update(permission, new QueryWrapper<SysPermission>().eq("id", vo.getId()));
        if (update != 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }

        // 如果授权表示也发生了改变，需要将用户标记起来，重新签发token
        if (!sysPermission.getPerms().equals(vo.getPerms())) {
            // 通过菜单权限查找到角色
            QueryWrapper<SysRolePermission> wrapper = new QueryWrapper<>();
            wrapper.eq("permission_id", sysPermission.getId());
            List<SysRolePermission> rolePermissions = rolePermissionMapper.selectList(wrapper);
            if (!rolePermissions.isEmpty()) {
                for (SysRolePermission rolePermission : rolePermissions) {
                    // 在通过角色找到用户
                    List<SysUserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<SysUserRole>().eq("role_id", rolePermission.getRoleId()));
                    if (userRoles != null) {
                        for (SysUserRole userRole : userRoles) {
                            // 标记到redis在中
                            redisUtil.set(Constant.JWT_REFRESH_KEY + userRole.getUserId(), userRole.getUserId(), tokenSetting.getAccessTokenExpireTime().toMillis(), TimeUnit.MILLISECONDS);
                            // 权限发生改变后,需要删除用户之前的缓存授权信息
                            redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userRole.getUserId());
                        }
                    }
                }
            }
        }
    }

    @Override
    public void deletePermissionByPermissionId(String permissionId) {
        // 先判断是否存在子集关联
        if (!getChildren(permissionId, selectAll()).isEmpty()) {
            throw new BusinessException(BaseResponseCode.ROLE_PERMISSION_RELATION);
        }
        // 执行删除菜单操作（逻辑删除）
        permissionMapper.delete(new QueryWrapper<SysPermission>().eq("id", permissionId));
        // 接触相关角色与该菜单的权限关联(物理删除)
        rolePermissionMapper.delete(new QueryWrapper<SysRolePermission>().eq("permission_id", permissionId));
        // 如果授权表示也发生了改变，需要将用户标记起来，重新签发token
        // 通过菜单权限查找到角色
        QueryWrapper<SysRolePermission> wrapper = new QueryWrapper<>();
        wrapper.eq("permission_id", permissionId);
        List<SysRolePermission> rolePermissions = rolePermissionMapper.selectList(wrapper);
        if (!rolePermissions.isEmpty()) {
            for (SysRolePermission rolePermission : rolePermissions) {
                // 在通过角色找到用户
                List<SysUserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<SysUserRole>().eq("role_id", rolePermission.getRoleId()));
                if (userRoles != null) {
                    for (SysUserRole userRole : userRoles) {
                        // 标记到redis在中
                        redisUtil.set(Constant.JWT_REFRESH_KEY + userRole.getUserId(), userRole.getUserId(), tokenSetting.getAccessTokenExpireTime().toMillis(), TimeUnit.MILLISECONDS);
                        // 权限发生改变后,需要删除用户之前的缓存授权信息
                        redisUtil.del(Constant.IDENTIFY_CACHE_KEY + userRole.getUserId());
                    }
                }
            }
        }
    }

    @Override
    public List<SysPermission> getPermissionsByRoleId(String userId) {
        List<String> roleIds = userRoleService.getRolesByUserId(userId);
        List<SysPermission> result = new ArrayList<>();
        if (!roleIds.isEmpty()) {
            for (String roleId : roleIds) {
                List<String> permissionIds = rolePermissionService.getPermissionIdsByRoleId(roleId);
                List<SysPermission> permission = permissionMapperII.getPermissionByIds(permissionIds);
                for (SysPermission sysPermission : permission) {
                    result.add(sysPermission);
                }
            }
        }
        return result;
    }

    /**
     * @description: 获取树形结构
     * @param all   所有菜单权限的集合
     * @param type  true:递归到按钮  false:递归到菜单
     * @return: java.util.List<com.demo.vo.resp.PermissionRespNodeVO>
     */
    private List<PermissionRespNodeVO> getTree(List<SysPermission> all,boolean type) {
        // 用于存储树形结构的集合
        List <PermissionRespNodeVO> treeList = new ArrayList<>();
        if (all.isEmpty()) {
            return treeList;
        }
        for (SysPermission sysPermission : all) {
            // 如果是最顶层，就遍历子集节点
            if (sysPermission.getPid().equals("0")) {
                PermissionRespNodeVO vo = new PermissionRespNodeVO();
                BeanUtils.copyProperties(sysPermission,vo);
                vo.setTitle(sysPermission.getName());
                // 使用递归的方法，遍历出所有的子节点
                if (type) {
                    vo.setChildren(getChildrenContainBtn(sysPermission.getId(), all));
                } else {
                    vo.setChildren(getChildren(sysPermission.getId(),all));
                }
                treeList.add(vo);
            }
        }
        return treeList;
    }

    /**
     * @description: 遍历出所有的子节点, 这里只递归到菜单处
     * @param id 当前id
     * @param all 全部的菜单数据
     * @return: java.util.List<com.demo.vo.resp.PermissionRespNodeVO>
     */
    private List<PermissionRespNodeVO> getChildren(String id, List<SysPermission> all) {
        List<PermissionRespNodeVO> treeList = new ArrayList<>();
        for (SysPermission sysPermission : all) {
            // 当id和父级id相同时,且type!=3就开始递归子集集合
            if (sysPermission.getPid().equals(id) && sysPermission.getType() != 3) {
                PermissionRespNodeVO vo = new PermissionRespNodeVO();
                BeanUtils.copyProperties(sysPermission,vo);
                vo.setTitle(sysPermission.getName());
                // 使用递归的方法，遍历出所有的子节点
                vo.setChildren(getChildren(sysPermission.getId(),all));
                treeList.add(vo);
            }
        }
        return treeList;
    }

    /**
     * @description: 遍历所有子节点，包括按钮
     * @param id
     * @param all
     * @return: java.util.List<com.demo.vo.resp.PermissionRespNodeVO>
     */
    private List<PermissionRespNodeVO> getChildrenContainBtn(String id, List<SysPermission> all) {
        List<PermissionRespNodeVO> treeList = new ArrayList<>();
        for (SysPermission sysPermission : all) {
            if (sysPermission.getPid().equals(id)) {
                PermissionRespNodeVO vo = new PermissionRespNodeVO();
                BeanUtils.copyProperties(sysPermission, vo);
                vo.setTitle(sysPermission.getName());
                // 使用递归的方法，遍历出所有的子节点
                vo.setChildren(getChildrenContainBtn(sysPermission.getId(), all));
                treeList.add(vo);
            }
        }
        return treeList;
    }
    /**
     * @description: 用于验证表单提交过来的所属菜单是否符合规范
     *  菜单权限类型(1:目录;2:菜单;3:按钮)
     *
     * 保存数据的时候需要对父级校验
     * 操作后的菜单类型是目录的时候 父级必须为目录
     * 操作后的菜单类型是菜单的时候，父类必须为目录类型
     * 操作后的菜单类型是按钮的时候 父类必须为菜单类型
     * @param permission
     * @return: void
     */
    private void verifyForm(SysPermission permission) {
        QueryWrapper<SysPermission> wrapper = new QueryWrapper<>();
        wrapper.eq("id", permission.getPid());
        // 查出父级节点,用于验证
        SysPermission parent = permissionMapper.selectOne(wrapper);
        switch (permission.getType()) {
            case 1:
                // 当添加的菜单权限类型是目录时，父级就只能是目录
                if (parent != null) {
                    if (parent.getType() != 1) {
                        throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_CATALOG_ERROR);
                    }
                }
                if (!permission.getPid().equals("0")) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_CATALOG_ERROR);
                }
                    break;
            case 2:
                // 当添加的菜单权限类型是菜单时，父级就只能是目录类型
                if (parent == null || parent.getType() != 1) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_MENU_ERROR);
                   }
                // 接口地址不能为空
                if (StringUtils.isEmpty(permission.getUrl())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_NOT_NULL);
                }
                    break;
            case 3:
                // 当添加的菜单权限是按钮时，父级就只能是菜单类型
                if (parent == null || parent.getType() != 2) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_BTN_ERROR);
                }
                // 接口地址不能为空
                if (StringUtils.isEmpty(permission.getUrl())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_NOT_NULL);
                }
                // 授权标识不能为空
                if (StringUtils.isEmpty(permission.getPerms())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_PERMS_NULL);
                }
                // 请求方式不能为空
                if (StringUtils.isEmpty(permission.getMethod())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_METHOD_NULL);
                }
                // 按钮标识不能为空
                if (StringUtils.isEmpty(permission.getCode())) {
                    throw new BusinessException(BaseResponseCode.OPERATION_MENU_PERMISSION_URL_CODE_NULL);
                }
                break;
        }
    }
}