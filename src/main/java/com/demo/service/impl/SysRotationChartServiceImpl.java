package com.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.pojo.SysRotationChart;
import com.demo.mapper.SysRotationChartMapper;
import com.demo.service.SysFileService;
import com.demo.service.SysRotationChartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.vo.req.AddRotationChartReqAddVO;
import com.demo.vo.req.RotationPageReqVO;
import com.demo.vo.req.UpdateRotationChartReqVO;
import com.demo.vo.resp.PageInfoVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 轮播图表结构 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-02-01
 */
@Service
public class SysRotationChartServiceImpl extends ServiceImpl<SysRotationChartMapper, SysRotationChart> implements SysRotationChartService {

    @Autowired
    private SysRotationChartMapper rotationChartMapper;

    @Autowired
    private SysFileService fileService;

    @Override
    public PageInfoVO<SysRotationChart> pageInfo(RotationPageReqVO vo) {
        QueryWrapper<SysRotationChart> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("sort");
        Page<SysRotationChart> page = new Page<>(vo.getPageNum(), vo.getPageSize());
        Page<SysRotationChart> rotationChartPage = rotationChartMapper.selectPage(page, wrapper);
        PageInfoVO<SysRotationChart> pageInfoVO = new PageInfoVO<>();
        pageInfoVO.setPageNum(vo.getPageNum());
        pageInfoVO.setPageSize(vo.getPageSize());
        pageInfoVO.setList(rotationChartPage.getRecords());
        return pageInfoVO;
    }

    @Override
    public int addRotationChart(AddRotationChartReqAddVO vo, String userId) {
        SysRotationChart rotationChart = new SysRotationChart();
        BeanUtils.copyProperties(vo, rotationChart);
        rotationChart.setCreateId(userId);
        int insert = rotationChartMapper.insert(rotationChart);
        if (insert < 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return insert;
    }

    @Override
    public int updateRotationChart(UpdateRotationChartReqVO vo, String userId) {
        SysRotationChart sysRotationChart = rotationChartMapper.selectById(vo.getId());
        List<String> fileUrl = new ArrayList<>();
        fileUrl.add(sysRotationChart.getFileUrl());
        // 删除之前上传的文件
        if (!sysRotationChart.getFileUrl().equals(vo.getFileUrl())) {
            fileService.deleteFile(fileUrl);
        }
        BeanUtils.copyProperties(vo, sysRotationChart);
        sysRotationChart.setUpdateId(userId);
        int update = rotationChartMapper.updateById(sysRotationChart);
        if (update < 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return update;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRotationChartById(List<String> ids) {
        int delete = 0;
        List<String> fileUrl = new ArrayList<>();
        if (ids != null && !ids.isEmpty()) {
            for (String id : ids) {
                List<SysRotationChart> sysRotationCharts = rotationChartMapper.selectList(new QueryWrapper<SysRotationChart>().eq("id", id));
                delete = rotationChartMapper.deleteById(id);
                for (SysRotationChart sysRotationChart : sysRotationCharts) {
                    fileUrl.add(sysRotationChart.getFileUrl());
                }
            }
        }
        // 还需删除之前上传的文件
        fileService.deleteFile(fileUrl);
        if (delete < 1) {
            throw new BusinessException(BaseResponseCode.OPERATION_ERROR);
        }
        return delete;
    }


    @Override
    public List<SysRotationChart> selectAll() {
        return rotationChartMapper.selectList(new QueryWrapper<SysRotationChart>().orderByDesc("sort"));
    }
}
