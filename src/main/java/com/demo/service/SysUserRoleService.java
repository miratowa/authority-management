package com.demo.service;

import com.demo.pojo.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.AssignUserRolesReqVO;
import com.demo.vo.resp.UserOwnRoleRespVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
public interface SysUserRoleService extends IService<SysUserRole> {
    /**
     * @description: 根据用户id获取用户拥有的角色信息
     * @param userId
     * @return: com.demo.vo.resp.UserOwnRoleRespVO
     */
    UserOwnRoleRespVO getUserOwnRole(String userId);

    /**
     * @description: 赋予用户角色
     * @param vo
     * @return: void
     */
    void AssignUserRoles(AssignUserRolesReqVO vo);

    /**
     * @description: 根据角色id查找到拥有该角色的用户id集合
     * @param roleId
     * @return: java.util.List<java.lang.String>
     */
    List<String> getUserIdsByRoleId(String roleId);

    /**
     * @description: 根据角色id删除用户
     * @param roleId
     * @return: int
     */
    int deleteUserByRoleId(String roleId);

    /**
     * @description: 根据用户id获取拥有的角色id集合
     * @param userId
     * @return: java.util.List<java.lang.String>
     */
    List<String> getRolesByUserId(String userId);
}
