package com.demo.service;

import com.demo.pojo.SysPermission;
import com.demo.pojo.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.vo.req.RolePermissionOperationReqVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
public interface SysRolePermissionService extends IService<SysRolePermission> {
    /**
     * @description: 批量添加角色权限
     * @param vo
     * @return: void
     */
    void addRolePermission(RolePermissionOperationReqVO vo);

    /**
     * @description: 根据角色id获取角色拥有的权限
     * @param roleId
     * @return: java.util.List<java.lang.String>
     */
    List<String> getPermissionIdsByRoleId(String roleId);

    /**
     * @description: 根据角色id删除角色锁拥有的权限
     * @param roleId
     * @return: int
     */
    int deletePermissionByRoleId(String roleId);


}
