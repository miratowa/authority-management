package com.demo.vo.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: 导航栏（不同的用户显示不同）
 * @create: 2020-12-29 20:30
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionRespNodeVO {
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "菜单权限名称")
    private String title;

    @ApiModelProperty(value = "接口地址")
    private String url;

    @ApiModelProperty("子集集合")
    private List children;

    @ApiModelProperty("默认展开层及目录")
    private boolean spread = true;

    @ApiModelProperty("是否选中,默认为false")
    private boolean checked;
}
