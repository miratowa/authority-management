package com.demo.vo.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: 主页返回数据
 * @create: 2020-12-29 20:29
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomeRespVO {
    @ApiModelProperty(value = "用户信息")

    private UserInfoRespVO userInfo;

    @ApiModelProperty(value = "目录菜单")
    private List<PermissionRespNodeVO> menus;

}
