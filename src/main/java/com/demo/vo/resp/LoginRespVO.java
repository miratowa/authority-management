package com.demo.vo.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @create: 2020-12-24 19:56
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRespVO {
    @ApiModelProperty("业务访问token")
    private String accessToken;

    @ApiModelProperty("业务token刷新凭证")
    private String refreshToken;

    @ApiModelProperty("用户id")
    private String userId;
}
