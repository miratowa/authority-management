package com.demo.vo.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: 部门树响应VO
 * @create: 2021-01-11 16:37
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeptRespNodeVO {
    @ApiModelProperty("部门id")
    private String id;

    @ApiModelProperty("部门名称")
    private String title;

    @ApiModelProperty("子集节点")
    private List children;

    @ApiModelProperty("是否展开层级关系")
    private boolean spread = true;
}
