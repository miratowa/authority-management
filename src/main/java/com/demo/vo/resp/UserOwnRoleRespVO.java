package com.demo.vo.resp;

import com.demo.pojo.SysRole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description: 用户拥有的角色vo
 * @create: 2021-01-12 19:27
 **/
@Data
public class UserOwnRoleRespVO {
    @ApiModelProperty("获取所有角色列表")
    private List<SysRole> allRole;

    @ApiModelProperty("根据用户id获取用户拥有的角色")
    private List<String> ownRoles;
}
