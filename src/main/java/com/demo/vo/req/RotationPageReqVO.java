package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @create: 2021-02-01 15:06
 **/
@Data
public class RotationPageReqVO {
    @ApiModelProperty("当前第几页")
    private Integer pageNum;

    @ApiModelProperty("当前页显示的条数")
    private Integer pageSize;
}
