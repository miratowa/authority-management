package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @create: 2021-01-24 14:32
 **/
@Data
public class LogPageReqVO {
    @ApiModelProperty(value = "当前页")
    private int pageNum;

    @ApiModelProperty(value = "每页显示的数量")
    private int pageSize;

    @ApiModelProperty(value = "用户操作动作")
    private String operation;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "账号")
    private String username;

    @ApiModelProperty(value = "开始时间")
    private String startTime;

    @ApiModelProperty(value = "结束时间")
    private String endTime;

}
