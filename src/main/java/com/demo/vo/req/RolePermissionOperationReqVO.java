package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: 角色权限操作要求VO
 * @create: 2021-01-11 10:45
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolePermissionOperationReqVO {
    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("角色id")
    private String roleId;

    @ApiModelProperty("菜单权限集合")
    private List<String> permissionIds;
}
