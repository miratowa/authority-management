package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @create: 2021-02-03 20:11
 **/
@Data
public class FilePageReqVO {
    @ApiModelProperty("当前页")
    private Integer pageNum;

    @ApiModelProperty("每页显示的记录条数")
    private Integer pageSize;
}
