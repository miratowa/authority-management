package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @create: 2021-01-12 11:04
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPageReqVO {
    @ApiModelProperty("当前页")
    private String pageNum;

    @ApiModelProperty("每页显示的条数 ")
    private String pageSize;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户名称")
    private String username;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("状态(0:正常,1:弃用)")
    private Integer status;

    @ApiModelProperty("开始时间")
    private String  startTime;

    @ApiModelProperty("结束时间")
    private String endTime;
}
