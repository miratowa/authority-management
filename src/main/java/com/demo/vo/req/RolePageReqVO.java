package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @description: 分页查询实体类
 * @create: 2021-01-07 10:37
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolePageReqVO {
    @ApiModelProperty("当前页")
    private String pageNum;

    @ApiModelProperty("每页显示的条数 ")
    private String pageSize;

    @ApiModelProperty("角色id")
    private String roleId;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("状态(1:正常,0:弃用)")
    private Integer status;

    @ApiModelProperty("开始时间")
    private String  startTime;

    @ApiModelProperty("结束时间")
    private String endTime;
}
