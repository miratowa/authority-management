package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @description:
 * @create: 2020-12-24 19:51
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginReqVO {
    @ApiModelProperty("用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;

    @ApiModelProperty("密码")
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty("登录来源:(1.Web,2.App)")

    private String type;
}
