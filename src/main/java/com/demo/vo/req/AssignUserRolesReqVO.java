package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @description: 赋予用户角色vo
 * @create: 2021-01-13 09:50
 **/
@Data
public class AssignUserRolesReqVO {
    @NotBlank(message = "用户id不能为空")
    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("角色id集合")
    private List<String> roleIds;
}
