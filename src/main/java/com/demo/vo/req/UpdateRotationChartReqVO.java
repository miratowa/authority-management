package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description:
 * @create: 2021-02-02 19:17
 **/
@Data
public class UpdateRotationChartReqVO {
    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("广告地址")
    @NotBlank(message = "广告地址不能为空")
    private String url;

    @ApiModelProperty("轮播图名称")
    @NotBlank(message = "轮播图名称不能为空")
    private String name;

    @ApiModelProperty("轮播图地址")
    @NotBlank(message = "轮播图地址不能为空")
    private String fileUrl;

    @ApiModelProperty("轮播图排序位置")
    @NotNull(message = "轮播图排序位置不能为空")
    private Integer sort;

    @ApiModelProperty("轮播图描述")
    private String description;
}
