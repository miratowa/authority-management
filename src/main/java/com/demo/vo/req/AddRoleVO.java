package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @description: 接受添加权限表单提交过来的数据
 * @create: 2021-01-11 10:18
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddRoleVO {
    @ApiModelProperty("角色名称")
    @NotBlank(message = "角色名称不能为空")
    private String name;

    @ApiModelProperty("备注")
    private String description;

    @ApiModelProperty("状态(1:正常0:弃用)")
    @NotNull(message = "角色状态不能为空")
    private Integer status;

    @ApiModelProperty("拥有的权限id集合")
    private List<String> permissionIds;
}
