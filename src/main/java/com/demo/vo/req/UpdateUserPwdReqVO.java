package com.demo.vo.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @create: 2021-01-26 16:24
 **/
@Data
public class UpdateUserPwdReqVO {
    @ApiModelProperty(value = "旧密码")
    private String oldPwd;

    @ApiModelProperty(value = "新密码")
    private String newPwd;

}
