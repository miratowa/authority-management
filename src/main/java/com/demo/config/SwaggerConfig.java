package com.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @description:
 * @create: 2020-12-23 19:02
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${swagger.enable}")
    private boolean enable;

    @Bean
    public Docket createDocket() {
        // 自定义header入口
        ArrayList<Parameter> parameters = new ArrayList<>();
        ParameterBuilder accessTokenBuilder = new ParameterBuilder();
        ParameterBuilder refreshTokenBuilder = new ParameterBuilder();

        accessTokenBuilder.name("authorization").description("测试动态传入accessToken入口")
                .modelRef(new ModelRef("String"))
                .parameterType("header")
                .required(false);

        refreshTokenBuilder.name("refreshToken").description("测试动态传入refreshToken入口")
                .modelRef(new ModelRef("String"))
                .parameterType("header")
                .required(false);

        parameters.add(accessTokenBuilder.build());
        parameters.add(refreshTokenBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.demo.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(parameters)
                .enable(enable);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Miratowa权限管理系统")
                .description("Miratowa权限管理系统接口文档")
                .version("1.0")
                .build();
    }
}
