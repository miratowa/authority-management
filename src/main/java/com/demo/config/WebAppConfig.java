package com.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description:
 * @create: 2021-02-01 13:51
 **/
@Configuration
public class WebAppConfig implements WebMvcConfigurer {
    @Value("${file.static-path}")
    private String fileStaticPath;

    @Value("${file.path}")
    private String filePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(fileStaticPath).addResourceLocations("file:"+filePath);
    }
}
