package com.demo.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * @description:
 * @create: 2020-12-24 19:15
 **/
@Configuration
@ConfigurationProperties(prefix = "jwt")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenSetting {
    private String secretKey, issuer;
    private Duration accessTokenExpireTime,refreshTokenExpireTime, refreshTokenExpireAppTime;
}
