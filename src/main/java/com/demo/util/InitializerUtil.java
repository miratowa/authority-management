package com.demo.util;

import org.springframework.stereotype.Component;

/**
 * @description: 代理类，用于初始化JWTUtil
 * @create: 2020-12-24 19:19
 **/
@Component
public class InitializerUtil {
    public InitializerUtil(TokenSetting tokenSetting) {
        JWTUtil.setJWTProperties(tokenSetting);
    }
}
