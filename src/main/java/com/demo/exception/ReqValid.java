package com.demo.exception;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @description:
 * @create: 2020-12-24 14:51
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqValid {
    @NotEmpty(message = "list集合不能为空")
    @ApiModelProperty("list集合")
    private List<String> list;

    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty("用户名")
    private String username;

    @NotNull(message = "年龄不能为空")
    @ApiModelProperty("年龄")
    private Integer age;
}
