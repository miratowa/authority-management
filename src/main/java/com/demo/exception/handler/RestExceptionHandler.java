package com.demo.exception.handler;

import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.util.DataResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * @description: 全局异常统一处理
 * @create: 2020-12-24 11:17
 **/
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {
    @ExceptionHandler(Exception.class)
    public DataResult test(Exception e) {
        return DataResult.getResult(BaseResponseCode.SYSTEM_ERROR);
    }

    @ExceptionHandler(BusinessException.class)
    public DataResult test2(BusinessException e) {
        return DataResult.getResult(e.getCode(), e.getMsg());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public DataResult test3(MethodArgumentNotValidException e) {
        // 获取全部的错误信息
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        // 遍历错误集合，并获取msg
        String error = "";
        for (ObjectError allError : allErrors) {
            error = allError.getDefaultMessage();
            break;
        }
        // 将错误信息返回给前端
        return DataResult.getResult(BaseResponseCode.VALIDATOR_ERROR.getCode(), error);
    }

    // 缺少权限
    @ExceptionHandler(UnauthorizedException.class)
    public DataResult unauthorizedException(UnauthorizedException e) {
        log.error("UnauthorizedException:{}",e);
        return DataResult.getResult(BaseResponseCode.NOT_PERMISSION);
    }
}
