package com.demo.shiro;

import com.alibaba.fastjson.JSON;
import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.util.DataResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.springframework.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @description: 自定义过滤器
 * @create: 2020-12-25 15:00
 **/
@Slf4j
public class CustomAccessControlFilter extends AccessControlFilter {
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        // 打印日志，记录接口地址和接口的请求方式
        log.info("request 接口地址:{}",request.getRequestURI());
        log.info("request 请求方式:{}",request.getMethod());
        // 获取token
        String accessToken = request.getHeader(Constant.ACCESS_TOKEN);
        try {
            if (StringUtils.isEmpty(accessToken)) {
                // 没有token,应该跳转到登录页面
                throw new BusinessException(BaseResponseCode.TOKEN_NOT_NULL);
            }
            getSubject(servletRequest, servletResponse).login(new CustomUsernamePasswordToken(accessToken));
        } catch (BusinessException e) {
            customResponse(servletResponse, e.getCode(), e.getMsg());
            return false;
        } catch (AuthenticationException e) {
            // 判断是否是自定义异常
            if (e.getCause() instanceof BusinessException) {
                BusinessException businessException = (BusinessException) e.getCause();
                // 如果是自定义异常，直接获取code和msg
                customResponse(servletResponse, businessException.getCode(), businessException.getMessage());
            } else {
                customResponse(servletResponse, BaseResponseCode.TOKEN_ERROR.getCode(), BaseResponseCode.TOKEN_ERROR.getMsg());
            }
            return false;
        } catch (Exception e) {
            customResponse(servletResponse, BaseResponseCode.SYSTEM_ERROR.getCode(), BaseResponseCode.SYSTEM_ERROR.getMsg());
            return false;
        }
        return true;
    }

    /**
     * @description: 自定义响应前端方法
     * @param response 响应数据
     * @param code  错误状态码
     * @param msg   错误信息
     * @return: void
     */
    private void customResponse(ServletResponse response , int code, String msg) {
        DataResult result = DataResult.getResult(code,msg);
        try {
            response.setContentType("application/json; charset=UTF-8");
            String json = JSON.toJSONString(result);
            response.getOutputStream().write(json.getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
