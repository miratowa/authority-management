package com.demo.shiro;

import com.demo.contants.Constant;
import com.demo.service.SysUserService;
import com.demo.service.impl.SysUserServiceImpl;
import com.demo.util.JWTUtil;
import com.demo.util.RedisUtil;
import io.jsonwebtoken.Claims;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @description: 自定义域
 * @create: 2020-12-28 11:08
 **/
public class CustomRealm extends AuthorizingRealm {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserServiceImpl userService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 读取jwt中的携带信息
        String accessToken = (String) principals.getPrimaryPrincipal();
        // 解析
        Claims claimsFromToken = JWTUtil.getClaimsFromToken(accessToken);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 判断是否有权限和角色信息
        if (claimsFromToken.get(Constant.PERMISSIONS_INFOS_KEY) != null) {
            info.addStringPermissions((Collection<String>) claimsFromToken.get(Constant.PERMISSIONS_INFOS_KEY));
        }
        if (claimsFromToken.get(Constant.ROLES_INFOS_KEY)!=null) {
            info.addRoles((Collection<String>) claimsFromToken.get(Constant.ROLES_INFOS_KEY));
        }
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        // 转换为我们自定义token
        CustomUsernamePasswordToken customUsernamePasswordToken = (CustomUsernamePasswordToken) token;
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(customUsernamePasswordToken.getPrincipal(), customUsernamePasswordToken.getCredentials(),this.getName());
        return info;
    }
}
