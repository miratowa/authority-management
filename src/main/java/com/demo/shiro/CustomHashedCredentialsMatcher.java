package com.demo.shiro;

import com.demo.contants.Constant;
import com.demo.exception.BusinessException;
import com.demo.exception.code.BaseResponseCode;
import com.demo.util.JWTUtil;
import com.demo.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

/**
 * @description: 自定义token验证器
 * 认证步骤分析：
 *          1.判断用户是否被锁定（后面有用户锁定功能，当用户锁定后我们会把该用户的id用redis给标记起来
 *          这是为了防止已经被锁定的用户继续访问我们需要用户认证的资源）
 *              否：下一步验证
 *              是：跳转到登录界面
 *
 *          2.判断用户是否被删除（后面我们有用户删除功能，当用户被删除后我们同样适用redis把该用户的id标记起来
 *          这里是为了防止已经被删除的用户继续访问我们需要用户认证的资源）
 *              否：下一步验证
 *              是：跳转到登录界面
 *
 *          3.校验access_token是否通过校验（这里把token是否正确和token是否已经过期合并在了一起，只要满足其中一个条件
 *          都会让前端调用我们的token刷新接口重新获取token）
 *              否：重新刷新token，拿到最新的token请求我们的接口
 *              是：下一步
 * @create: 2020-12-28 11:43
 **/
@Slf4j
public class CustomHashedCredentialsMatcher extends HashedCredentialsMatcher {
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        CustomUsernamePasswordToken customUsernamePasswordToken = (CustomUsernamePasswordToken) token;
        String accessToken = (String) customUsernamePasswordToken.getCredentials();
        // 拿到UserId
        String userId = JWTUtil.getUserId(accessToken);
        log.info("doCredentialsMatch....userId={}",userId);
        // 判断用户是否被删除
        if (redisUtil.hasKey(Constant.DELETED_USER_KEY + userId)) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_HAS_DELETED_ERROR);
        }
        // 判断用户是否被封禁
        if (redisUtil.hasKey(Constant.ACCOUNT_LOCK_KEY + userId)) {
            throw new BusinessException(BaseResponseCode.ACCOUNT_LOCK_tip);
        }
        // 判断用户是否是自动退出
        if( redisUtil.hasKey(Constant.JWT_ACCESS_TOKEN_BLACKLIST + accessToken)) {
            throw new BusinessException(BaseResponseCode.TOKEN_ERROR);
        }
        // 校验token
        if (!JWTUtil.validateToken(accessToken)) {
            throw new BusinessException(BaseResponseCode.TOKEN_PAST_DUE);
        }
        // 判断用户是否被标记，如果被标记应该刷新token
        if (redisUtil.hasKey(Constant.JWT_REFRESH_KEY+userId)) {
            // 判断token是否自动刷新了
            if (redisUtil.getExpire(Constant.JWT_REFRESH_KEY + userId, TimeUnit.MILLISECONDS) > JWTUtil.getRemainingTime(accessToken)) {
                throw new BusinessException(BaseResponseCode.TOKEN_PAST_DUE);
            }
        }

        return true;
    }
}
