package com.demo.shiro;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;

/**
 * @description:
 * @create: 2020-12-28 14:38
 **/
@Configuration
public class ShiroConfig {
    @Bean
    public CustomHashedCredentialsMatcher customHashedCredentialsMatcher() {
        return new CustomHashedCredentialsMatcher();
    }

    @Bean
    public CustomRealm customRealm() {
        CustomRealm realm = new CustomRealm();
        realm.setCredentialsMatcher(customHashedCredentialsMatcher());
        return realm;
    }

    @Bean
    public SecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(customRealm());
        return securityManager;
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean filterFactoryBean = new ShiroFilterFactoryBean();
        // 将安全管理器配置到过滤器工厂中
        filterFactoryBean.setSecurityManager(securityManager);

        LinkedHashMap<String, Filter> map = new LinkedHashMap<>();
        map.put("token", new CustomAccessControlFilter());
        // 将我们自定义的过滤器加入到过滤器工厂中
        filterFactoryBean.setFilters(map);

        LinkedHashMap<String, String> openMap = new LinkedHashMap<>();
        // 配置不需要拦截的地址

        // 登录相关请求
        openMap.put("/", "anon");
        openMap.put("/api/login", "anon");
       // openMap.put("/api/user/token", "anon");
        openMap.put("/users/info", "anon");
       openMap.put("/users/pwd", "anon");
       openMap.put("/api/rotations/add", "anon");
       openMap.put("/api/rotations/delete", "anon");
        // 无需认证的文件下载
        //openMap.put("/api/file/download/*", "anon");
       // openMap.put("/api/rotations", "anon");
        openMap.put("/upload/image/**", "anon");
        openMap.put("/toHome", "anon");
       // openMap.put("/users/info", "anon");
        openMap.put("/404", "anon");
        openMap.put("/main", "anon");
        //openMap.put("/users/password", "anon");
        //openMap.put("/rotations/rotation", "anon");
        //openMap.put("/rotations/show", "anon");
        openMap.put("/logs", "anon");
        openMap.put("/menus", "anon");
        openMap.put("/roles", "anon");
        openMap.put("/depts", "anon");
        openMap.put("/users", "anon");
        openMap.put("/files", "anon");
        // swagger
        openMap.put("/swagger/**", "anon");
        openMap.put("/v2/api-docs", "anon");
        openMap.put("/swagger-ui.html", "anon");
        openMap.put("/swagger-resources/**", "anon");
        openMap.put("/treetable-lay/**", "anon");
        openMap.put("/webjars/**", "anon");
        openMap.put("/favicon.ico", "anon");
        openMap.put("/captcha.jpg", "anon");
        // 静态资源
        openMap.put("/js/**", "anon");
        openMap.put("/layui/**", "anon");
        openMap.put("/X-admin/**", "anon");
        openMap.put("/css/**", "anon");
        openMap.put("/images/**", "anon");
        // druid sql监控
        openMap.put("/druid/**", "anon");
        openMap.put("/**", "token,authc");
        filterFactoryBean.setFilterChainDefinitionMap(openMap);
        return filterFactoryBean;
    }

    // 开启AOP
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

    // shiro整合thymelaf
    @Bean
    public ShiroDialect getShiroDialect() {
        return new ShiroDialect();
    }
}
