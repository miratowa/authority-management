package com.demo.mapper;

import com.demo.pojo.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Repository
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
