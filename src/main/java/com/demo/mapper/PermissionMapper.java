package com.demo.mapper;

import com.demo.pojo.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @create: 2021-01-28 22:23
 **/
@Repository
@Mapper
public interface PermissionMapper {
    List<SysPermission> getPermissionByIds(List<String> ids);

}
