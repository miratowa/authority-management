package com.demo.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.demo.pojo.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.vo.req.RolePageReqVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<SysRole> rolePageInfo(RolePageReqVO vo);
}
