package com.demo.mapper;

import com.demo.pojo.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Repository
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
