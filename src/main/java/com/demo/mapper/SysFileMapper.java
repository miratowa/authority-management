package com.demo.mapper;

import com.demo.pojo.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 文件信息存储表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-01-31
 */
@Repository
public interface SysFileMapper extends BaseMapper<SysFile> {

}
