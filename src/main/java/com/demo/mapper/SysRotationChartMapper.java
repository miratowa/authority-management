package com.demo.mapper;

import com.demo.pojo.SysRotationChart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 轮播图表结构 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-02-01
 */
@Repository
public interface SysRotationChartMapper extends BaseMapper<SysRotationChart> {

}
