package com.demo.mapper;

import com.demo.pojo.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Repository
public interface SysLogMapper extends BaseMapper<SysLog> {

}
