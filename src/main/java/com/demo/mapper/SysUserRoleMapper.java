package com.demo.mapper;

import com.demo.pojo.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-12-29
 */
@Repository
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
